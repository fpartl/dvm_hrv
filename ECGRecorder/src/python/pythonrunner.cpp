#include "pythonrunner.h"

PythonRunner::PythonRunner(QObject *parent) : QObject(parent) { }

bool PythonRunner::isReady() {
    QProcess python;

    python.start(PYTHON_EXECUTABLE, QStringList() << "--version");
    python.waitForFinished(-1);

    return python.exitStatus() == QProcess::NormalExit && python.exitCode() == EXIT_SUCCESS;
}

bool PythonRunner::hasModule(const QString &moduleName) {
    QProcess python;

    python.start(PYTHON_EXECUTABLE, QStringList() << "-c" << QString("\"import %1\"").arg(moduleName));
    python.waitForFinished(-1);

    return python.exitStatus() == QProcess::NormalExit && python.exitCode() == EXIT_SUCCESS;
}

bool PythonRunner::hasModules(const QStringList &modulesList) {
    for (QString modul : modulesList) {
        if (!hasModule(modul))
            return false;
    }

    return true;
}

bool PythonRunner::runScript(const QString &script, const QStringList &params, QString *output) {
    if (!scriptExists(script))
        return false;

    QStringList processParams = QStringList() << script << params;

    Console::writeLine(QString("Spoustim: %1\n").arg(startScriptCommand(script, params)));

    QProcess python;
    python.setWorkingDirectory(QFileInfo(script).path());
    python.start(PYTHON_EXECUTABLE, processParams);
    python.waitForFinished(-1);

    bool success = python.exitStatus() == QProcess::NormalExit && python.exitCode() == EXIT_SUCCESS;
    QString pythonOutput;

    if (success) {
       pythonOutput = python.readAllStandardOutput();
    }
    else {
        Console::writeLine("VYKONAVANI SKRIPTU SKONCILO CHYBOU!");
        pythonOutput = python.readAllStandardError();
    }

    Console::write(pythonOutput);
    Console::flush();

    if (output)
        *output = pythonOutput;

    return success;
}

bool PythonRunner::scriptExists(const QString &script) {
    if (script.isEmpty())
        return false;

    QFileInfo info(script);
    return info.exists() && info.isFile() && info.suffix().compare(PYTHON_SCRIPT_SUFFIX) == 0;
}

QString PythonRunner::startScriptCommand(const QString &script, const QStringList &params) {
    if (script.isEmpty())
        return QString();

    QString paramsString;
    QStringListIterator i(params);
    while (i.hasNext()) {
        paramsString.append("\"" + i.next() + "\"");

        if (i.hasNext())
            paramsString.append(" ");
    }

    return QString("%1 %2 %3").arg(PYTHON_EXECUTABLE).arg("\"" + script + "\"").arg(paramsString);
}
