#ifndef PYTHONRUNNER_H
#define PYTHONRUNNER_H

#include <QObject>
#include <QStringList>
#include <QStringListIterator>
#include <QProcess>
#include <QFileInfo>

#include "libfp/console.h"
#include "config.h"

/* Přípona platného pythonovského skriptu. */
#define PYTHON_SCRIPT_SUFFIX "py"

/*!
 * \brief Třída PythonRunner
 *
 * Třída slouží ke spouštění pythonovských skriptů pomocí příkazu PYTHON_EXECUTABLE. Metody třídy dovolují
 * kontrolu dostupnosti interpreta a modulů jazyka Python a spuštění skriptů včetně parametrů.
 */
class PythonRunner : public QObject {
    Q_OBJECT

public:
    /*!
     * \brief PythonRunner Konstruktor třídy.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     */
    explicit PythonRunner(QObject *parent = nullptr);

    /*!
     * \brief isReady Metoda zkontroluje, zda je v hostitelském operačním systému dostupný interpret jazyka
     *                Python.
     * \return True, pokud byl Python nalezen, jinak false.
     */
    bool isReady();

    /*!
     * \brief hasModule Metoda zkontroluje, zda zadaný modul je nainstalován a je tedy možné jej využít
     *                  v pythonovských skriptech.
     * \param moduleName Název požadovaného modulu Pythonu.
     * \return True, pokud modul je nainstalován, jinak false.
     */
    bool hasModule(const QString &moduleName);

    /*!
     * \brief hasModules Metoda zkontroluje, zda má metoda nainstalovány všechny zadané moduly.
     * \param modulesList Seznam požadovaných modulů.
     * \return True, pokud jsou moduly nainstalovány, jinak false.
     */
    bool hasModules(const QStringList &modulesList);

    /*!
     * \brief runScript Metoda spustí zadaný pythonovský skript pomocí interpretu jazyka Python s definovanými
     *                  parametry. Interpret bude spuštěn jako samostatný proces a tato metoda nevrátí, dokud
     *                  interpretace nedoběhne, tj. volání je blukující.
     * \param script Umístění pythonovského skriptu, který bude spuštěn.
     * \param params Seznam parametrů interpretu/skriptu.
     * \param output Ukazatel na řetězec, kam bude zapsán standartní výstup (stdout) interpreta jazyka Python.
     * \return True, pokud interpret doběhl bez chyb (návratová hodnota byla 0), jinak false.
     */
    bool runScript(const QString &script, const QStringList &params, QString *output = nullptr);

    /*!
     * \brief scriptExists Metoda zkonstroluje, zda zadaný skript skutečně existuje.
     * \param script Cesta ke kontrolovanému skriptu.
     * \return True, pokud skript existuje, jinak false.
     */
    static bool scriptExists(const QString &script);

private:
    /*!
     * \brief startScriptMessage Metoda vytvoří řetězec, který představuje hlášení o spuštění daného skriptu
     *                           s určenými parametry.
     * \param script Název spuštěného skriptu.
     * \param params Parametry spuštění skriptu.
     * \return Řětězec, který informuje o spuštění skriptu.
     */
    QString startScriptCommand(const QString &script, const QStringList &params);
};

#endif
