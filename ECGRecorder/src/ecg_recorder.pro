QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

DEFINES += QT_DEPRECATED_WARNINGS

#Použití knihovny DAQmx
linux-g++ | linux-g++-64 | linux-g++-32 {
    INCLUDEPATH += "/usr/local/natinst/nidaqmxbase/include"
    LIBS += -L/usr/local/lib64 -lnidaqmxbase
}
win32 {
    INCLUDEPATH += "C:/Program Files (x86)/National Instruments/NI-DAQ/DAQmx ANSI C Dev/include"
    LIBS += "C:/Program Files (x86)/National Instruments/NI-DAQ/DAQmx ANSI C Dev/lib/msvc/NIDAQmx.lib"
}

SOURCES += \
        core/ecganalyzer.cpp \
        ecg/ecgloader.cpp \
        ecg/outwriterworker.cpp \
        ecg/usbworker.cpp \
        ecg/usbworkersimulator.cpp \
        libfp/console.cpp \
        libfp/workerrunner.cpp \
        libfp/workerthread.cpp \
        main.cpp \
        output/ekgoutfile.cpp \
        output/textfile.cpp \
        python/pythonrunner.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    config.h \
    core/ecganalyzer.h \
    ecg/ecgloader.h \
    ecg/outwriterworker.h \
    ecg/usbworker.h \
    ecg/usbworkersimulator.h \
    libfp/cyclicbuffer.h \
    output/csvfile.h \
    libfp/console.h \
    libfp/eventproducer.h \
    libfp/sharedqueue.h \
    libfp/workerrunner.h \
    libfp/workerthread.h \
    output/ekgoutfile.h \
    output/textfile.h \
    python/pythonrunner.h
