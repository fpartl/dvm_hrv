#ifndef OUTWRITERWORKER_H
#define OUTWRITERWORKER_H

#include <QObject>
#include <QString>

#include "config.h"
#include "libfp/workerthread.h"
#include "libfp/sharedqueue.h"
#include "output/csvfile.h"

/*!
 * \brief Třída OutWriterWorker
 *
 * Pracovní vlákno, jehož účelem je bufferování dat ze vstupní fronty. Po každém naplněním cyklického bufferu,
 * jsou data dávkově zapsány do výstupního souboru, který je dán ukazatelem v kontruktoru třídy. Výkon pracovního
 * vlákna je ukončen uzavřením pracovní fronty.
 */
class OutWriterWorker : public WorkerThread {
    Q_OBJECT

public:
    /*!
     * \brief OutWriterWorker Konstruktor třídy.
     * \param queue Fronta vstupních dat.
     * \param outFile Výstupní soubor.
     * \param bufferSize Velikost použitého vyrovnávacího bufferu.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění dynamicky alokovaného
     *               objektu.
     */
    explicit OutWriterWorker(SharedQueue<EKGChunkPointer> *queue, CSVFile<ekg_sample> *outFile, int bufferSize = BUFFER_SIZE, QObject *parent = nullptr);

    /*!
     * \brief isReady Metoda konstroluje, zda je připravena, jak vstupní frotna, tak výstupní souboru.
     * \return True, pokud jsou všechny komponenty připraveny a vlákno je možné spustit, jinak false.
     */
    bool isReady();

    /*!
     * \brief isReady Metoda zjistí, zda by bylo vlákno připraveno s danými parametry.
     * \param queue Fronta vstupních dat.
     * \param outFile Výstupní soubor.
     * \param bufferSize Velikost použitého vyrovnávacího bufferu.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění dynamicky alokovaného
     *               objektu.
     * \return True, pokud by vlákno bylo připraveno s danými parametry, jinak false.
     */
    static bool isReady(SharedQueue<EKGChunkPointer> *queue, CSVFile<ekg_sample> *outFile, int bufferSize = BUFFER_SIZE, QObject *parent = nullptr);

    /*!
     * \brief run Kód pracovního vlákna postupně čte data ze vstupní fronty, zapisuje je do cyklického bufferu a po
     *            jeho naplnění jsou data pomocí metody OutWriterWorker::flushBuffer() zapsána do výstupního bufferu.
     */
    void run() override;

private:
    SharedQueue<EKGChunkPointer> *m_queue;          //!< Vstupní fronta.
    CSVFile<ekg_sample> *m_outFile;                 //!< Výstupní soubor.
    int m_bufferSize;                               //!< Velikost vnitřního cyklického bufferu.

    /*!
     * \brief flushBuffer Volání metody spustí zápis celého cyklického bufferu do výstupního souboru.
     * \param Ukazatel na buffer, který bude vyprazdňován.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool flushBuffer(QVector<EKGChunkPointer> * const buffer);
};

#endif
