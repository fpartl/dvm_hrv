#include "ecgloader.h"

ECGLoader::ECGLoader(QString usbPort, int frequency, int loadDuration,
                     CSVFile<ekg_sample> * const outFile, QObject *parent) : WorkerRunner(parent)
{
    m_usbPort = usbPort;
    m_frequency = frequency;
    m_loadDuration = loadDuration;
    m_outFile = outFile;
}

bool ECGLoader::isReady() {
    return INPUT_DEVICE::isReady(&m_sharedQueue, m_usbPort, m_frequency)
                && OutWriterWorker::isReady(&m_sharedQueue, m_outFile);
}

bool ECGLoader::isWorking() const {
    return hasRunningWorkers();
}

bool ECGLoader::start() {
    if (isWorking())
        return false;

    m_sharedQueue.reset();

    if (!isReady())
        return false;

    runWorker(new INPUT_DEVICE(&m_sharedQueue, m_usbPort, m_frequency, m_loadDuration), true);
    runWorker(new OutWriterWorker(&m_sharedQueue, m_outFile), true);

    return true;
}

void ECGLoader::allWorkersDone() {
    emit ekgLoadingFinished();
}

void ECGLoader::handleError(const QString &message) {
    Console::writeLine(QString("CHYBA! %1").arg(message));
    m_sharedQueue.stop();
}

void ECGLoader::handleWarning(const QString &message) {
    Console::writeLine(QString("VAROVANI! %1").arg(message));
    m_sharedQueue.stop();
}

void ECGLoader::handleNotice(const QString &message) {
    Console::writeLine(message);
}
