#ifndef USBWORKERSIMULATOR_H
#define USBWORKERSIMULATOR_H

#include <QObject>
#include <QVector>
#include <QVectorIterator>

#include "libfp/workerthread.h"
#include "libfp/sharedqueue.h"
#include "output/csvfile.h"
#include "config.h"

/* Vstupní CSV soubor. */
#define INPUT_CSV_FILE "/home/frantisek/Plocha/test.csv"

/*!
 * \brief Třída USBWorkerSimulator
 *
 * Třída představuje simulaci čtení dat A/D převodníku připojenému k USB portu. Slouží k testování
 * prakticky celé aplikace vyjímaje samotné třídy USBWorker.
 */
class USBWorkerSimulator : public WorkerThread {
    Q_OBJECT

public:
    /*!
     * \brief USBWorker Konstruktor třídy.
     * \param outQueue Fronta výstupních úseků zaznamenaných vzorků EKG signálu.
     * \param inputPort Popis vstupního USB portu, ke kterému je připojen A/D převodník.
     * \param frequency Vzorkovací frekvence zaznamenávaného signálu.
     * \param duration Přednastavená doba po kterou bude záznam probíhat (nevalidní pokud je <= 0) -- ignorovaná hodnota.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     */
    explicit USBWorkerSimulator(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration = -1, QObject *parent = nullptr);

    /*
     * Desktruktor třídy.
     */
    ~USBWorkerSimulator() override;

    /*!
     * \brief isReady Metoda kontroluje, za je vlákno připraveno ke spuštění.
     * \return True, pokud je vlákno připraveno ke spuštění, jinak false.
     */
    bool isReady();

    /*!
     * \brief isReady Metoda zjistí, zda by bylo vlákno připraveno s danými parametry.
     * \param outQueue Fronta výstupních úseků zaznamenaných vzorků EKG signálu.
     * \param inputPort Popis vstupního USB portu, ke kterému je připojen A/D převodník.
     * \param frequency Vzorkovací frekvence zaznamenávaného signálu.
     * \param duration Přednastavená doba po kterou bude záznam probíhat (nevalidní pokud je <= 0) -- ignorovaná hodnota.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     * \return True, pokud by bylo vlákno s těmito parametry připraveno, jinak false.
     */
    static bool isReady(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration = -1, QObject *parent = nullptr);

    /*!
     * \brief isRecording Metoda vrací aktuální stav nahrávání.
     * \return True, pokud se aktuálně nahrává, jinak false.
     */
    bool isRecording() const;

    /*!
     * \brief run Metoda spustí nahrávání pouze na danou dobu danou v sekundách. Po uplynutí bude vlákno zastaveno.
     * \param duration Délka zázanamu v sekundách.
     */
    void start(int duration);

    /*!
     * \brief start Metoda spustí nahrávání na předvolenou dobu m_duration, která je dána v sekundách. Pokud je tato
     *              hodnota nevalidní, vlákno se vůbec nespustí.
     */
    void start();

    /*!
     * \brief stop Metoda zastaví nahrávání.
     */
    void stop();

private:
    SharedQueue<EKGChunkPointer> *m_outQueue;       //!< Výstupní fronta.
    QString m_inputPort;                            //!< Popis vstupního USB portu, ke kterému je připojen A/D převodník.
    int m_frequency;                                //!< Vzorkovací frekvence zaznamenávaného signálu.
    bool m_isRecording;                             //!< Proměnná, která říká, zda se aktuálně nahrává nebo ne.

    /*!
     * \brief run Metoda, která spustí připravené vlákno, tj. pokud volání metody isReady vrátí hodnotu
     *            false, metoda neprovede žádnou akci.
     */
    void run() override;
};

#endif
