#include "outwriterworker.h"

OutWriterWorker::OutWriterWorker(SharedQueue<EKGChunkPointer> *queue, CSVFile<ekg_sample> *outFile, int bufferSize, QObject *parent) : WorkerThread(parent)
{
    m_queue = queue;
    m_outFile = outFile;
    m_bufferSize = bufferSize;
}

bool OutWriterWorker::isReady() {
    return m_queue && m_outFile && m_outFile->exists(true) && m_bufferSize > 0;
}

bool OutWriterWorker::isReady(SharedQueue<EKGChunkPointer> *queue, CSVFile<ekg_sample> *outFile, int bufferSize, QObject *parent) {
    Q_UNUSED(parent);

    return queue && outFile && outFile->exists(true) && bufferSize > 0;
}

void OutWriterWorker::run() {
    if (!isReady())
        return;

    emit notice(QString("Zaznamenavam EKG signal do souboru %1.").arg(m_outFile->filePath()));

    QVector<EKGChunkPointer> buffer;
    while (EKGChunkPointer ekgChunk = m_queue->dequeue()) {
        buffer.append(ekgChunk);

        if (buffer.size() == m_bufferSize) {
            if (!flushBuffer(&buffer))
                return;
        }
    }

    if (!buffer.isEmpty())
        flushBuffer(&buffer);
}

bool OutWriterWorker::flushBuffer(QVector<EKGChunkPointer> * const buffer) {
    if (!buffer)
        return false;

    for (EKGChunkPointer chunk : *buffer) {
        if (WRITE_EKG_IN_ONE_ROW ? !m_outFile->write(*chunk) : !m_outFile->writeLine(*chunk)) {
            emit error(QString("Data se nedari zapsat do vystupniho souboru %1.").arg(m_outFile->filePath()));
            return false;
        }
    }

    buffer->clear();
    return true;
}
