#ifndef USBWORKER_H
#define USBWORKER_H

#include <QObject>
#include <QString>
#include <QSharedPointer>
#include <QVector>
#include <QMutex>
#include <QDateTime>

#include "config.h"
#include "libfp/workerthread.h"
#include "libfp/sharedqueue.h"
#include <QTimer>

/* Include knihovny pro přístup k A/D převodníku na OS MS Windows.  */
#ifdef __MINGW32__
    #include <NIDAQmx.h>

    /* Definice použitých funkcí pro práci s knihovnou. */
    #define DAQMX_READ          DAQmxReadAnalogF64
    #define DAQMX_STOP          DAQmxStopTask
    #define DAQMX_CLEAR         DAQmxClearTask
    #define DAQMX_CREATE        DAQmxCreateTask
    #define DAQMX_CREATE_CHAN   DAQmxCreateAIVoltageChan
    #define DAQMX_CREATE_TIMING DAQmxCfgSampClkTiming
    #define DAQMX_START         DAQmxStartTask
#endif

/* Include knihovny pro přístup k A/D převodníku na OS Linux. */
#ifdef __linux__
    #include <NIDAQmxBase.h>

    /* Definice použitých funkcí pro práci s knihovnou. */
    #define DAQMX_READ          DAQmxBaseReadAnalogF64
    #define DAQMX_STOP          DAQmxBaseStopTask
    #define DAQMX_CLEAR         DAQmxBaseClearTask
    #define DAQMX_CREATE        DAQmxBaseCreateTask
    #define DAQMX_CREATE_CHAN   DAQmxBaseCreateAIVoltageChan
    #define DAQMX_CREATE_TIMING DAQmxBaseCfgSampClkTiming
    #define DAQMX_START         DAQmxBaseStartTask
#endif

/* Maximální hodnota zaznamenávaného signálu. */
#define MAX_VALUE 10

/* Minimální hodnota zaznamenávaného signálu. */
#define MIN_VALUE -10

#define CONSOLE_TIME_FORMAT "hh:mm:ss"

/*!
 * \brief Třída USBWorker
 *
 * Třída představuje pracovní vlákno, které pomocí knihovny NIDAQmx čte data ze zadaného
 * USB portu, ke kterému je připojen A/D převodník NI USB-6008. Data získaná z převodníku
 * zapisuje po dávkách do výstupní frony ve formě sdíleného ukazatele. Jedna dávka obsahuje
 * frequency prvků, tj. jednu sekundu záznamu.
 *
 * VAROVÁNÍ!
 * Jedním z parametrů konstruktoru je ukazatel na sdílenou frontu, skrze kterou objekt této
 * třídy předává zaznamenané vzorky signálu. Vždy když pracovní vlákno záznamu doběhne nebo
 * je násilně přerušeno, je tato fronta uzavřena. Pro opětovné spuštění záznam je tedy třeba
 * nad frontou volat nejlépe metody SharedQueue::reset() a SharedQueue::start().
 */
class USBWorker : public WorkerThread {
    Q_OBJECT

public:
    /*!
     * \brief USBWorker Konstruktor třídy.
     * \param outQueue Fronta výstupních úseků zaznamenaných vzorků EKG signálu.
     * \param inputPort Popis vstupního USB portu, ke kterému je připojen A/D převodník.
     * \param frequency Vzorkovací frekvence zaznamenávaného signálu.
     * \param duration Přednastavená doba po kterou bude záznam probíhat (nevalidní pokud je <= 0).
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     */
    explicit USBWorker(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration = -1, QObject *parent = nullptr);

    /*
     * Desktruktor třídy.
     */
    ~USBWorker() override;

    /*!
     * \brief isReady Metoda kontroluje, za je vlákno připraveno ke spuštění.
     * \return True, pokud je vlákno připraveno ke spuštění, jinak false.
     */
    bool isReady();

    /*!
     * \brief isReady
     * \return
     */
    /*!
     * \brief isReady Metoda zjistí, zda by byl objekt připraven k záznamu EKG signálu s těmito parametry.
     * \param outQueue Fronta výstupních úseků zaznamenaných vzorků EKG signálu.
     * \param inputPort Popis vstupního USB portu, ke kterému je připojen A/D převodník.
     * \param frequency Vzorkovací frekvence zaznamenávaného signálu.
     * \param duration Přednastavená doba po kterou bude záznam probíhat (nevalidní pokud je <= 0).
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     * \return True, pokud by bylo vlákno s těmito parametry připraveno, jinak false.
     */
    static bool isReady(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration = -1, QObject *parent = nullptr);

    /*!
     * \brief isRecording Metoda vrací aktuální stav nahrávání.
     * \return True, pokud se aktuálně nahrává, jinak false.
     */
    bool isRecording() const;

    /*!
     * \brief start Metoda spustí nahrávání na předvolenou dobu m_duration, která je dána v sekundách. Pokud je tato
     *              hodnota nevalidní, vlákno se vůbec nespustí.
     */
    void start();

    /*!
     * \brief stop Metoda zastaví nahrávání.
     */
    void stop();

    /*!
     * \brief duration Metoda získá aktuálně nastavenou předvolenou hodnotu délky nahrávání EKG signálu.
     * \return Přednastavená hodnota délky nahrávání EKG signálu.
     */
    int duration() const;

    /*!
     * \brief frequency Metoda získá aktuálně nastavenou předvolenou vzorkovací frekvenci nahrávání EKG signálu.
     * \return Přednastavená hodnota vzorkovací frekvence EKG signálu.
     */
    int frequency() const;

private:
    SharedQueue<EKGChunkPointer> *m_outQueue;                       //!< Výstupní fronta.
    QString m_inputPort;                                            //!< Popis vstupního USB portu, ke kterému je připojen A/D převodník.
    int m_frequency;                                                //!< Vzorkovací frekvence zaznamenávaného signálu.
    int m_duration;                                                 //!< Předvolená doba, kterou se bude nahrávat signál.
    bool m_isRecording;                                             //!< Proměnná, která říká, zda se aktuálně nahrává nebo ne.

    /*!
     * \brief run Metoda, která spustí připravené vlákno, tj. pokud volání metody isReady vrátí hodnotu
     *            false, metoda neprovede žádnou akci.
     */
    void run() override;

    /*!
     * \brief emitStartNotice Metoda vypíše, že záznam signálu započal a spolu s touto informací vypíše
     *                        čas spuštění a čas ukončení záznamu ve formátu CONSOLE_TIME_FORMAT.
     */
    void emitStartNotice();

    /*!
     * \brief initializeNIDAQmx Metoda provede inicializaci nové úlohy pro záznam vzorků ze zařízení NIDAQmx.
     * \param task Ukazatel na ukazatel TaskHandle, který bude inicializován.
     */
    void initializeNIDAQmx(TaskHandle *task);
};

#endif
