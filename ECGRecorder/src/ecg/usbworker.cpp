#include "usbworker.h"
#include "libfp/console.h"

USBWorker::USBWorker(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration, QObject *parent) : WorkerThread(parent) {
    m_outQueue = outQueue;
    m_inputPort = inputPort;
    m_frequency = frequency;
    m_duration = duration;
    m_isRecording = false;
}

USBWorker::~USBWorker() {
    if (m_outQueue && m_outQueue->isOpened())
        m_outQueue->stop();
}

bool USBWorker::isReady() {
    return m_outQueue && m_outQueue->isOpened() && !m_isRecording && m_frequency > 0 && !m_inputPort.isEmpty();
}

bool USBWorker::isReady(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration, QObject *parent) {
    Q_UNUSED(duration);
    Q_UNUSED(parent);

    return outQueue && outQueue->isOpened() && !inputPort.isEmpty() && frequency > 0;
}

bool USBWorker::isRecording() const {
    return m_isRecording;
}

void USBWorker::start() {
    if (m_duration <= 0) {
        emit error("Predvolena hodnota doby nahravani musi byt ostre vetsi nez nula a je udana v sekundach.");
        return;
    }

    QThread::start();
}

void USBWorker::stop() {
    m_isRecording = false;
}

int USBWorker::duration() const {
    return m_duration;
}

int USBWorker::frequency() const {
    return m_frequency;
}

void USBWorker::run() {
    if (!isReady())
        return;

    if (m_duration > 0) {
        // * 60 * 1000 kvůli převodu na milisekundy.
        QTimer::singleShot(m_duration * 60 * 1000, this, &USBWorker::stop);
    }

    emitStartNotice();
    m_isRecording = true;

    TaskHandle AITaskHandle = nullptr;
    initializeNIDAQmx(&AITaskHandle);

    while (isRecording()) {
        EKGChunkPointer vector = EKGChunkPointer::create();
        vector->resize(m_frequency);

        int ret = DAQMX_READ(AITaskHandle, m_frequency, 10, // 10 je výchozí hodnota timeoutu
                        DAQmx_Val_GroupByChannel, vector->data(), static_cast<unsigned int>(m_frequency), nullptr, nullptr);
        if (ret != 0) {
            emit error(QString("Cteni z prevodniku skoncilo chybou %1!").arg(ret));
            break;
        }

        m_outQueue->enqueue(vector);
        QThread::msleep(static_cast<unsigned long>(m_frequency));
    }

    DAQMX_STOP(AITaskHandle);
    DAQMX_CLEAR(AITaskHandle);

    m_outQueue->stop();
    emit notice(QString("Ukoncuji nahravani EKG signalu."));
}

void USBWorker::emitStartNotice() {
    QDateTime currentTime = QDateTime::currentDateTime();
    QDateTime estFinish = QDateTime::currentDateTime();

    // * 60 kvůli převodu na sekundy (Qt nemá addMinutes nebo něco takového...)
    estFinish = estFinish.addSecs(m_duration * 60);

    emit notice(QString("Spoustim nahravani %1 %2 EKG signalu (cas spusteni: %3 -> cas ukonceni: %4).")
                    .arg(m_duration).arg(m_duration > 1 ? "minut" : "minuty")
                    .arg(currentTime.toString(CONSOLE_TIME_FORMAT))
                    .arg(estFinish.toString(CONSOLE_TIME_FORMAT))
        );
}

void USBWorker::initializeNIDAQmx(TaskHandle *task) {
    if (!task)
        return;

    /*
     * Inicializace ulohy.
     * Nastaveni portu, ze ktereho budeme cist data (plus dalsi atributy - min. a max. hodnoty, apod.)
     * Nastaveni casovace, ktery nam zajisti pozadovanou vzorkovaci frekvenci.
     * Spusteni ulohy.
     */

    DAQMX_CREATE("", task);
    DAQMX_CREATE_CHAN(*task, m_inputPort.toStdString().c_str(), "", DAQmx_Val_Cfg_Default, -10, 10, DAQmx_Val_Volts, "");
    DAQMX_CREATE_TIMING(*task, nullptr, m_frequency, DAQmx_Val_Rising, DAQmx_Val_ContSamps, static_cast<unsigned long long>(m_frequency));
    DAQMX_START(*task);
}
