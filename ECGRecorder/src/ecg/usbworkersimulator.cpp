#include "usbworkersimulator.h"

USBWorkerSimulator::USBWorkerSimulator(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration, QObject *parent) : WorkerThread(parent) {
    Q_UNUSED(duration)

    m_outQueue = outQueue;
    m_inputPort = inputPort;
    m_frequency = frequency;
    m_isRecording = false;
}

USBWorkerSimulator::~USBWorkerSimulator() {
    if (m_outQueue && m_outQueue->isOpened())
        m_outQueue->stop();
}

bool USBWorkerSimulator::isReady() {
    return m_outQueue && m_outQueue->isOpened() && !m_isRecording && m_frequency > 0 && !m_inputPort.isEmpty();
}

bool USBWorkerSimulator::isReady(SharedQueue<EKGChunkPointer> *outQueue, QString inputPort, int frequency, int duration, QObject *parent) {
    Q_UNUSED(duration);
    Q_UNUSED(parent);

    return outQueue && outQueue->isOpened() && frequency > 0 && !inputPort.isEmpty();
}

bool USBWorkerSimulator::isRecording() const {
    return m_isRecording;
}

void USBWorkerSimulator::start(int duration) {
    Q_UNUSED(duration);

    QThread::start();
}

void USBWorkerSimulator::start() {
    QThread::start();
}

void USBWorkerSimulator::stop() {
    m_isRecording = false;
}

void USBWorkerSimulator::run() {
    m_isRecording = true;
    emit notice(QString("Spouštím simulaci záznamu signálu ze souboru %1").arg(INPUT_CSV_FILE));

    CSVFile<ekg_sample> inputFile(INPUT_CSV_FILE);

    QVector<ekg_sample> data = inputFile.readAll();
    QVectorIterator<ekg_sample> i(data);

    while (i.hasNext()) {
        EKGChunkPointer vector = EKGChunkPointer::create();

        for (int k = 0; k < m_frequency && i.hasNext(); k++)
            vector->append(i.next());

        m_outQueue->enqueue(vector);
    }

    emit notice(QString("Ukončuji simulaci záznamu signálu se souboru %1").arg(INPUT_CSV_FILE));
}
