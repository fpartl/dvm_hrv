#ifndef ECGLOADER_H
#define ECGLOADER_H

#include <QObject>
#include <QSharedPointer>
#include <QTimer>

#include "config.h"
#include "libfp/sharedqueue.h"
#include "libfp/workerrunner.h"
#include "libfp/console.h"
#include "ecg/usbworker.h"
#include "ecg/outwriterworker.h"
#include "output/csvfile.h"

#include "ecg/usbworkersimulator.h"

/*!
 * \brief Třída EKGLoader
 *
 * Třída spravuje dvě pracovní vlákna, která zaznamenávají vzorky EKG signálu a následně tento signál
 * ukládají do daného výstupního souboru.
 */
class ECGLoader : public WorkerRunner {
    Q_OBJECT

public:
    /*!
     * \brief EKGLoader Konstruktor třídy.
     * \param usbPort Popisovač vstupního USB portu.
     * \param frequency Vzorkovací frekvence.
     * \param loadDuration Délka záznamu EKG signálu v minutách.
     * \param outFile Ukazatel na výstupní soubor.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění dynamicky alokovaného
     *               objektu.
     */
    explicit ECGLoader(QString usbPort, int frequency, int loadDuration, CSVFile<ekg_sample> * const outFile, QObject *parent = nullptr);

    /*!
     * \brief isReady Konstroluje, zda jsou všechny komponenty připraveny ke pouštění záznamu EKG signálu.
     * \return True, pokud jsou všechny komponenty připraveny ke spuštění záznamu EKG signálu, jinak false.
     */
    bool isReady();

    /*!
     * \brief isWorking Metoda zkontroluje, zda je aktuálně spuštěno nahrávání vzorků EKG signálu.
     * \return True, pokud je nahrávání aktivní, jinak false.
     */
    bool isWorking() const;

    /*!
     * \brief start Metoda spuští pracovní vlákna, tj. záznam EKG signálu do výstupního souboru.
     * \return True, pokud bylo nahrávání úspěšně spuštěno, jinak false.
     */
    bool start();

private:
    SharedQueue<EKGChunkPointer> m_sharedQueue;     //!< Fronta sdílená mezi pracovními vlákny.
    QString m_usbPort;                              //!< Popisovač vstupního USB portu.
    int m_frequency;                                //!< Vzorkovací frekvence.
    int m_loadDuration;                             //!< Délka záznamu EKG signálu v minutách.
    CSVFile<ekg_sample> *m_outFile;                 //!< Ukazatel na výstupní soubor.

protected:
    /*!
     * \brief allWorkersDone Metoda, která je spuoštěna, pokud všechna pracovní vlákna dokončí svou činnost, tj. nahrávání dokončeno.
     */
    void allWorkersDone() override;

public slots:
    /*!
     * \brief handleError Slot, jehož úlohou je reakce na chybové hlášení.
     * \param message Obsah chybového hlášení.
     */
    void handleError(const QString &message) override;

    /*!
     * \brief handleWarning Slot, jehož úlohou je reakce na varování.
     * \param message Obsah varovného hlášení.
     */
    void handleWarning(const QString &message) override;

    /*!
     * \brief handleNotice Slot, jehož úlohou je reakce na obecnou upomínku.
     * \param message Obsah upomínky.
     */
    void handleNotice(const QString &message) override;

signals:
    /*!
     * \brief ekgLoadingFinished Signál, který je emitován, pokud je záznam EKG signlu dokončen, tj. dojedou všechna pracovní vlákna.
     */
    void ekgLoadingFinished();
};

#endif
