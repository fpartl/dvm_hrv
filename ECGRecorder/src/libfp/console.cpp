#include "console.h"

void Console::write(const QString &message) {
    if (message.isEmpty())
        return;

    std::cout << message.toStdString();
}

void Console::writeLine(const QString &message) {
    std::cout << message.toStdString() << std::endl;
}

QString Console::readLine() {
    std::string input;

    std::cin >> input;
    return QString::fromStdString(input);
}

bool Console::printConfirm(const QString &question) {
    if (question.isEmpty())
        return false;

    Console::write(QString("%1 (y/n):").arg(question));

    QString answer = Console::readLine();
    QRegularExpression re(QString() += Console::Agreement);
    QRegularExpressionMatch match = re.match(answer);

    return match.hasMatch();
}

void Console::flush() {
    std::cout.flush();
}
