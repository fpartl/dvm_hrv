#ifndef EVENTPRODUCER_H
#define EVENTPRODUCER_H

#include <QObject>
#include <QVector>
#include <QMutableVectorIterator>

template <class T>
/*!
 * \brief Třída EventProducer
 *
 * Třída reprezentuje obecný tzv. subject v návrhovém vzoru Observer. Generickým parametrem T je myšlen
 * typ, jakým bude pro konkrétní implementaci této třídy její možný observer.
 */
class EventProducer {

public:
    /*
     * Virtuální destruktor.
     */
    virtual ~EventProducer() = default;

    /*!
     * \brief registerObserver Metoda zapíše nového observera do vektoru registrovaných
     *                         observerů. Následně zavolá metodu connectObserver, kde konkrétní implementace
     *                         této virtuální metody provede propojení daných slotů, skrze které je později
     *                         realizován přenos aktuálních dat.
     * \param observer Ukazatel na nového observera.
     */
    void registerObserver(T *observer);

    /*!
     * \brief removeObserver Metoda odstraní zadaného observera z vektoru registrovaných observerů. V případě, že
     *                       observer není registrován, metoda neprovede žádnou akci. Po odstranění observeru
     *                       z vektoru registrovaných observerů je zavolána metoda dicConnectObserver, jejíž
     *                       implementace mají za úkol odpojit všechna propojení, která byla navázána v metodě
     *                       registerObserver.
     * \param poor Ukazatel na observera k odstranění.
     */
    void removeObserver(T *poor);

private:
    QVector<T *> m_observers;       //!< Vektor registrovaných observerů.

protected:
    /*!
     * \brief connectObserver Virtuální metoda, jejíž implemetace mají za úkol propojit všchny požadované signály
     *                        a sloty mezi potomkem této třídy (Objectem) a jejím observerem.
     * \param observer Ukazatel na nového observera.
     */
    virtual void connectObserver(T *observer) = 0;

    /*!
     * \brief disConnectObserver Virtuální metoda, jejíž implementace mají za úkol odpojit všechna spojení vytvořená
     *                           v metodě connectObserver;
     * \param poor Ukazatel na observera, který je určen k odpojení.
     */
    virtual void disConnectObserver(T *poor) = 0;
};

template <class T>
void EventProducer<T>::registerObserver(T *observer) {
    if (!observer)
        return;

    m_observers.append(observer);
    connectObserver(observer);
}

template <class T>
void EventProducer<T>::removeObserver(T *poor) {
    if (!poor)
        return;

    QMutableVectorIterator<T *> i(m_observers);
    while (i.hasNext()) {
        T *i_val = i.next();

        if (i_val == poor) {
            disConnectObserver(poor);
            i.remove();

            return;
        }
    }
}

#endif
