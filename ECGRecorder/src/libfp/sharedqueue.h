#ifndef SHAREDQUEUE_H
#define SHAREDQUEUE_H

#include <QObject>
#include <QQueue>
#include <QWaitCondition>
#include <QMutex>
#include <limits>

template <class T>
/*!
 * \brief Třída SharedQueue
 *
 * Tato třída je implementací fronty, která řeší problém producent-konzument, tj. předávání
 * zpracovávaných dat mezi pracovními vlákny.
 */
class SharedQueue : public QObject {

public:
    /*!
     * \brief SharedQueue Konstruktor třídy.
     * \param maxSize Maximální počet položek fronty. Pokud bude zadána záporná hodnota, velikost bude "neomezená".
     * \param sleepProducent Nastaví, zda se má producent při pokusu vložit položku do plné fronty uspat, nebo
     *                       jeho volání funkce enque vrátí hodnotu false.
     * \param parent Ukazatel na rodiče objektu (kvůli uvolnění paměti).
     */
    explicit SharedQueue(int maxSize = -1, bool sleepProducent = true, QObject *parent = nullptr);

    /*!
     * \brief enqueue Metoda, která vloží prvek na konec fronty. Tuto metodu tedy volají producenti. Pokud je atribut
     *                m_sleepProducent nastaven na true a producent se pokusí vložit prvek do plné fronty, pak je
     *                volající vlákno uspáno, dokud nebude dostupná volná pozice ve frontě.
     * \param item Položka, určená ke vložení do fronty.
     * \return Metoda vrací false jenom tehdy, pokud je atribut m_sleepProducent nastaven na true a producent se pokusí
     *         vložit položku do plné fronty. Jinak funkce vrací true.
     */
    bool enqueue(T item);

    /*!
     * \brief dequeue Metoda pro odebrání položky ze začátku fronty. V případě, že se konzument pokusí odebrat prvek
     *                z prázdné fronty, je volající vlákno uspáno, dokud není ve frontě dostupný prvek ke konzumaci.
     * \return Položka fronty.
     */
    T dequeue();

    /*!
     * \brief stop Metoda nastaví atribut m_running na hodnotu false a provede probuzení všech čekajících producentů
     *             a konzumentů. Nastavení příznaku na false způsobí zakáz dalšího přidávání prvků do fronty.
     *             Odebírání prvků bude dále možné, dokud se fronta nevyprázdní. Pak je zakázáno uspávání konzumentů.
     */
    void stop();

    /*!
     * \brief start Metoda nastaví atribut m_running na hodnotu truea provede probuzení všech čekající producentů
     *              a konzumentů. Nastavení příznaku na true způsobí povolení dalšího přidávání prvků do fronty.
     *              Volání metody nijak nevlivní možnost odebírání prvků z fronty konzumenty.
     */
    void start();

    /*!
     * \brief clear Metoda odstraní všechny prvky z fronty.
     */
    void clear();

    /*!
     * \brief reset Metoda zastaví akce sdílené fronty a odstraní všechny její prvky. Tím je fronta uvedena do původního stavu.
     *              Jedná se o adekvátní volání jako v případě stop() a clear(). Doporučuje se tuto metodu volat pokud ji již
     *              nepoužívají žádná vlákna. Následným voláním metody start() je fronta připravena k opětovnému použití.
     */
    void reset();

    /*!
     * \brief isOpened Metoda zjistí, zda je fronta otevřena pro vkládání nových záznamů.
     * \return True, pokud je fronta otevřena vkládání nových záznamů, jinak false.
     */
    bool isOpened();

private:
    int m_maxSize;                          //!< Maximální velikost fronty.
    bool m_sleepProducent;                  //!< Příznak, který povolí či zakáže možnost uspání producentů.
    bool m_running;                         //!< Příznak běhu předávání dat. Pokud je false, není možné přidávat další položky.
    QQueue<T> m_queue;                      //!< Fronta předávaných položek.
    QWaitCondition m_sleepingProducers;     //!< Fronta čekajících producentů.
    QWaitCondition m_sleepingConsumers;     //!< Fronta čekajících konzumentů.
    QMutex m_mutex;                         //!< Mutex k ošetření kritické sekce.
    QMutex m_runSetMutex;                   //!< Mutex k ošetření souběhu při nastavování příznaku m_running.
};

template <class T>
SharedQueue<T>::SharedQueue(int maxSize, bool sleepProducent, QObject *parent) : QObject(parent) {
    m_running = true;
    m_sleepProducent = sleepProducent;

    m_maxSize = (maxSize > 0) ? maxSize : std::numeric_limits<int>::max();

    if (maxSize > 0)
        m_queue.reserve(m_maxSize);
}

template<class T>
bool SharedQueue<T>::enqueue(T item) {
    m_mutex.lock();

    while (m_queue.count() >= m_maxSize && m_running) {
        if (!m_sleepProducent) {
            m_mutex.unlock();
            return false;
        }

        m_sleepingProducers.wait(&m_mutex);
    }

    if (!m_running) {
        m_sleepingProducers.wakeAll();
        m_mutex.unlock();
        return false;
    }

    m_queue.enqueue(item);

    m_sleepingConsumers.wakeAll();
    m_mutex.unlock();
    return true;
}

template<class T>
T SharedQueue<T>::dequeue() {
    m_mutex.lock();

    while (m_queue.isEmpty() && m_running)
        m_sleepingConsumers.wait(&m_mutex);

    if (!m_running && m_queue.isEmpty()) {
       m_sleepingConsumers.wakeAll();
       m_mutex.unlock();
       return nullptr;
    }

    T result = m_queue.dequeue();

    m_sleepingProducers.wakeAll();
    m_mutex.unlock();
    return result;
}

template<class T>
void SharedQueue<T>::stop() {
    m_runSetMutex.lock();
    m_running = false;
    m_runSetMutex.unlock();

    m_sleepingProducers.wakeAll();
    m_sleepingConsumers.wakeAll();
}

template<class T>
void SharedQueue<T>::start() {
    m_runSetMutex.lock();
    m_running = true;
    m_runSetMutex.unlock();

    m_sleepingProducers.wakeAll();
    m_sleepingConsumers.wakeAll();
}

template<class T>
void SharedQueue<T>::clear() {
    m_mutex.lock();
    m_queue.clear();
    m_mutex.unlock();
}

template<class T>
void SharedQueue<T>::reset() {
    start();
    clear();
}

template<class T>
bool SharedQueue<T>::isOpened() {
    return m_running;
}

#endif
