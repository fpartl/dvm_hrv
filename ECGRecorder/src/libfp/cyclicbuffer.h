#ifndef CYCLICBUFFER_H
#define CYCLICBUFFER_H

#include <QObject>
#include <QVector>

template <class T>
/*!
 * \brief Třída CyclicBuffer
 *
 * Třída představuje obecný cyklický buffer, u kterého je třeba velikost jeho interního bufferu
 * zadat již při konstrukci objektu. Buffer je šablonovaný, nicméně využívá kontejnerů QVector<T>,
 * které vyžadují, aby jejich třídy jejich prvků měly defaultní konstruktor. Tuto vlastnost
 * bohužel tato třída dědí.
 */
class CyclicBuffer : public QObject {

public:
    /*!
     * \brief CyclicBuffer Kontruktor třídy. Velikost interního bufferu je třeba zadat ještě před
     *                     konstrukcí objektu. Pokud pak dojde k vložení většího množštví dat než
     *                     je kapacita bufferu, nebudou se provádět žádné akce.
     * \param bufferSize Velikost interního bufferu.
     * \param parent Ukazatel na rodiče objektu, který lze dle notace Qt použít k automatickému
     *               uvolnění dynamicky alokovaného objektu.
     */
    explicit CyclicBuffer(int bufferSize, QObject *parent = nullptr);

    /*
     * Destruktor třídy, která uvolní dynamicky alokovaný interní buffer.
     */
    ~CyclicBuffer();

    /*!
     * \brief write Metoda do cyklického bufferu zapíše jednu položku.
     * \param item Položka vložená do cycklického bufferu.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool write(const T &item);

    /*!
     * \brief write Metoda zapíše do interního bufferu daná data. Pokud se data do interního bufferu
     *              nevejdou, metoda vypíše warování pomocí qWarning a neprovede žádnou akci.
     * \param data Data určená k zápisu do kruhového bufferu.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool write(const QVector<T> &data);

    /*!
     * \brief write Přetížená metoda, pomocí které lze zapsat data určená libovolným ukazelem do paměti.
     * \param data Ukazatel na vkládaná data.
     * \param bufferSize Počet vkládaných položek.
     * \return True, pokud bylo vložení provedeno úspěšně, jinak false.
     */
    bool write(const T *data, int bufferSize);

    /*!
     * \brief isBuffered Metoda zkontroluje, zda buffer obsahuje alepospoň estSize položek.
     * \param estSize Počet požadavaných položek v interním bufferu.
     * \return Truek, pokud interní buffer obsahuje alespoň estSize položek, jinak false.
     */
    bool isBuffered(int estSize);

    /*!
     * \brief takeData Metoda provede extrakci size položek z interního bufferu, které vrátí
     *                 jako vektor. Pokud buffer tento počet položek neobsahuje, metoda vrátí
     *                 prázdný vektor.
     * \param size Počet extrahovaných položek z interního bufferu.
     * \return Vektor extrahovaných položek nebo prázdný vektor, pokud položky nejsou k dispozici.
     */
    QVector<T> takeData(int size);

    /*!
     * \brief exstractAll Metoda provede extrakci všech položek z interního bufferu, které vrátí
     *                    jako vektor.
     * \return Vektor odebraných položek.
     */
    QVector<T> takeAll();

    /*!
     * \brief extractDataPtr Metoda provede extrakci size prvků z kruhového bufferu, pro které dynamicky alokuje
     *                       potřebný paměťový prostor a ukazatel na něj vrátí. Pokud požadované množství prvků
     *                       v interním bufferu není, metoda vrátí hodnotu nullptr.
     * \param bufferSize Počet extrahovaných položek z interního bufferu.
     * \return Ukazatel na extrahovaná data nebo nullptr.
     */
    T *extractDataPtr(int bufferSize);

    /*!
     * \brief clear Metoda vyprázdní interní buffer.
     */
    void clear();

    /*!
     * \brief reset Metoda vyprázdní interní buffer (přetížená metoda).
     */
    void reset();

    /*!
     * \brief isEmpty Metoda zjistí, zda je interní buffer prázný.
     * \return True, pokud je interní buffer prázdný, jinak false.
     */
    bool isEmpty();

    /*!
     * \brief bufferSize Metoda vrátí celkovou veliksot interního bufferu.
     * \return Velikost interního bufferu.
     */
    int bufferSize();

    /*!
     * \brief size Metoda vrací počet prvků v bufferu.
     * \return Počet prvků v bufferu.
     */
    int size();

private:
    T *m_buffer;            //!< Dynamicky alokovaný interní buffer.
    int m_bufferSize;       //!< Velikost interního bufferu.
    int m_first;            //!< Index prvního prvku v interním bufferu.
    int m_next;             //!< Index, kam se bude zapisovat další prvek v interním bufferu.
};

template<class T>
CyclicBuffer<T>::CyclicBuffer(int size, QObject *parent) : QObject(parent) {
    m_bufferSize = size;
    m_buffer = new T[size];
    m_first = m_next = 0;
}

template<class T>
CyclicBuffer<T>::~CyclicBuffer() {
    delete [] m_buffer;
}

template<class T>
bool CyclicBuffer<T>::write(const T &item) {
    if (1 > m_bufferSize)
        return false;

    if (!memcpy(m_buffer + m_next, &item, sizeof(T)))
        return false;

    m_next = (m_next + 1) % m_bufferSize;

    return true;
}



template<class T>
bool CyclicBuffer<T>::write(const QVector<T> &data) {
    if (data.size() > m_bufferSize)
        return false;

    int newPosition = m_next + data.size();
    if (newPosition > m_bufferSize) {
        int restCount = m_bufferSize - m_next;
        int onStart = data.size() - restCount;

        memcpy(m_buffer + m_next, data.constData(), static_cast<size_t>(restCount) * sizeof(T));
        memcpy(m_buffer, data.constData() + restCount, static_cast<size_t>(onStart) * sizeof(T));
    }
    else memcpy(m_buffer + m_next, data.constData(), static_cast<size_t>(data.size()) * sizeof(T));

    m_next = newPosition % m_bufferSize;

    return true;
}

template<class T>
bool CyclicBuffer<T>::write(const T *data, int size) {
    if (size > m_bufferSize)
        return false;

    int newPosition = m_next + size;
    if (newPosition > m_bufferSize) {
        int restCount = m_bufferSize - m_next;
        int onStart = size - restCount;

        memcpy(m_buffer + m_next, data, static_cast<size_t>(restCount) * sizeof(T));
        memcpy(m_buffer, data + restCount, static_cast<size_t>(onStart) * sizeof(T));
    }
    else memcpy(m_buffer + m_next, data, static_cast<size_t>(size) * sizeof(T));

    m_next = newPosition % m_bufferSize;

    return true;
}

template<class T>
bool CyclicBuffer<T>::isBuffered(int estSize) {
    return size() >= estSize;
}

template<class T>
QVector<T> CyclicBuffer<T>::takeData(int size) {
    if (!isBuffered(size))
        return QVector<T>();

    QVector<T> extracted;
    extracted.reserve(size);

    if (m_first + size > m_bufferSize) {
        int rest = m_bufferSize - m_first;
        int onStart = size - rest;

        std::copy(m_buffer + m_first, m_buffer + m_first + rest, std::back_inserter(extracted));
        std::copy(m_buffer, m_buffer + onStart, std::back_inserter(extracted));
    }
    else std::copy(m_buffer + m_first, m_buffer + m_first + size, std::back_inserter(extracted));

    m_first = (m_first + size) % m_bufferSize;

    return extracted;
}

template<class T>
QVector<T> CyclicBuffer<T>::takeAll() {
    if (size() <= 0)
        return QVector<T>();

    QVector<T> extracted;
    extracted.reserve(size());

    if (m_first + size() > m_bufferSize) {
        int rest = m_bufferSize - m_first;
        int onStart = size() - rest;

        std::copy(m_buffer + m_first, m_buffer + m_first + rest, std::back_inserter(extracted));
        std::copy(m_buffer, m_buffer + onStart, std::back_inserter(extracted));
    }
    else std::copy(m_buffer + m_first, m_buffer + m_first + size(), std::back_inserter(extracted));

    m_first = (m_first + size()) % m_bufferSize;

    return extracted;
}

template<class T>
T *CyclicBuffer<T>::extractDataPtr(int size) {
    if (!isBuffered(size))
        return nullptr;

    T *extracted = new T[size];

    if (m_first + size > m_bufferSize) {
        int rest = m_bufferSize - m_first;
        int onStart = size - rest;

        memcpy(extracted, m_buffer + m_first, static_cast<size_t>(rest) * sizeof(T));
        memcpy(extracted + rest, m_buffer, static_cast<size_t>(onStart) * sizeof(T));
    }
    else memcpy(extracted, m_buffer + m_first, static_cast<size_t>(size) * sizeof(T));

    m_first = (m_first + size) % m_bufferSize;

    return extracted;
}

template<class T>
void CyclicBuffer<T>::clear() {
    m_first = m_next;
}

template<class T>
void CyclicBuffer<T>::reset() {
    m_first = m_next;
}

template<class T>
bool CyclicBuffer<T>::isEmpty() {
    return m_first == m_next;
}

template<class T>
int CyclicBuffer<T>::bufferSize() {
    return m_bufferSize;
}

template<class T>
int CyclicBuffer<T>::size() {
    int sizeNotOver = m_next - m_first;
    int sizeOverflow = m_bufferSize - m_first + m_next;

    return (m_first <= m_next) ? sizeNotOver : sizeOverflow;
}

#endif
