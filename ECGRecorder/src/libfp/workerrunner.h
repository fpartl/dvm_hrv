﻿#ifndef WORKERRUNNER_H
#define WORKERRUNNER_H

#include <QObject>
#include <QString>
#include <QVector>

#include "console.h"

/* Čas v milisekundách, po který se čeká na ukončení násilně ukončeného pracovního vlákna než bude zničeno */
#define MAX_WAIT_TIME 2000

class WorkerThread;

/*!
 * \brief Třída WorkerRunner
 *
 * Třída obsahuje virtuální metody, které musí odvozená třída realizovat, aby se mohla spouštět
 * vlákna typu WorkerThread. Tato pracovní vlákna pak jsou evidována a řízena, lze z nich odchytávat
 * definované signály a sleduje se počet jich aktivních.
 */
class WorkerRunner : public QObject {
    Q_OBJECT

public:
    /*!
     * \brief NoticeHandler Konstruktor třídy.
     * \param parent Ukazatel na rodiče objektu, který lze dle notace Qt využít k automatické destrukci objektu.
     */
    explicit WorkerRunner(QObject *parent = nullptr);

    /*
     * Destruktor třídy.
     */
    virtual ~WorkerRunner() = 0;

    /*!
     * \brief hasRunningWorkers Metoda kontroluje, zda instance třídy aktuálně spravuje nějaká spuštěná vlákna třídy
     *                          WokerThread.
     * \return True, pokud nějaká vlánka třídy jsou aktivní, jinak false.
     */
    bool hasRunningWorkers() const;

    /*!
     * \brief stopAllWorkers Metoda vyzve všechna pracovní vlákna u ukončení. Pokud se vlákna do MAX_WAIT_TIME
     *                       milisekund nezastaví, budou ukončení násilně pomocí metody QThread::terminate().
     */
    void stopAllWorkers();

private:
    QVector<WorkerThread *> m_workers;      //!< Vektor spuštěných pracovních vláken typu WorkerThread.

    /*!
     * \brief workerFinished Metoda je automaticky volána při ukončení nějakého z pracovních vláken. Toto
     *                       vlákno je následně odstraněno z vektoru spuštěných vláken a v případě, že vektor
     *                       již neobsahuje žádná další aktivní vlákna, zavolá se metoda allWorkersDone, která
     *                       je realizována v odovozené třídě.
     */
    void workerFinished();

protected:
    /*!
     * \brief runWorker Metoda, která přijímá ukazatel na vytvořené pracovní vlákno. Metoda propojí potřebné signály,
     *                  přidá jej do vektoru aktivních pracovních vláken a nakonec vlákno spustí.
     * \param worker Ukazatel na spouštěné pracovní vlákno typu WorkerThread.
     * \param deleteLater Příznak, který spojí ukončení vlákna s jeho následnou destrukcí.
     */
    void runWorker(WorkerThread* worker, bool deleteLater = true);

    /*!
     * \brief allWorkersDone Metoda, která je volána při ukončení posledního aktivního vlákna ve vektoru m_workers.
     */
    virtual void allWorkersDone() = 0;

public slots:
    /*!
     * \brief handleError Slot, jehož úlohou je reakce na chybové hlášení.
     * \param message Obsah chybového hlášení.
     */
    virtual void handleError(const QString& message) = 0;

    /*!
     * \brief handleWarning Slot, jehož úlohou je reakce na varování.
     * \param message Obsah varovného hlášení.
     */
    virtual void handleWarning(const QString& message) = 0;

    /*!
     * \brief handleNotice Slot, jehož úlohou je reakce na obecnou upomínku.
     * \param message Obsah upomínky.
     */
    virtual void handleNotice(const QString& message) = 0;

signals:
    /*!
     * \brief allWorkersFinished Signál, který informuje o tom, že všechna pracovní vlákna doběhla.
     */
    void allWorkersFinished();
};

#endif
