#ifndef CONSOLE_H
#define CONSOLE_H

#include <QObject>
#include <QString>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <iostream>

/*!
 * \brief Třída Console
 *
 * Třída obsahuje kolekci statických metod, které dovolují snadnou práci s konzolovým rozhraním
 * mých aplikací. Framework Qt podle všeho neobsahuje přímou možnost těchto operací, tak jsem si
 * je implementoval sám...
 */
class Console : public QObject {
    Q_OBJECT

public:
    /*!
     * \brief write Metoda do výstupního proudu stdout vypíše zadaný text. Pokud je vstupní text prázdný,
     *              neprovede se žádná akce.
     * \param message Zpráva, která má být vytištěna do konzole.
     */
    static void write(const QString &message);

    /*!
     * \brief writeLine Metoda od výstupního proudu stdou vypíše zadaný text, který automaticky ukončí znakem
     *                  endl. Pokud je vstupní text prázdný, pouze odřádkuje.
     * \param message Zpráva, která má být vytištěna do konzole.
     */
    static void writeLine(const QString &message);

    /*!
     * \brief readLine Metoda ze vstupního proudu přečte jeden řádek, který vrátí ve formě objektu třídy QString.
     *                 Volání této metody je blokující.
     * \return Jeden řádek přečtený ze vstupního proudu.
     */
    static QString readLine();

    /*!
     * \brief printConfirm Metoda vypíše zadanou otázku do výstupního proudu pomocí metody Console::print a přečte
     *                     odpověď uživatele pomocí metody Console::readLine. Pokud odpověď odpovídá regulárnímu
     *                     výrazu AGREE_REGEX, metoda vrací hodnotu true, tj. uživatel se zprávou souhlasí, jinak false.
     *                     Pokud je vstupní otázka prádná, metoda neprovede žádnou akci a ihned vrátí hodnotu false.
     * \param question Otázka, která je uživateli vypsána do výstupního proudu stdout.
     * \return True, pokud odpověď uživatele odpovídá regulárnímu výrazu AGREE_REGEX, jinak false.
     */
    static bool printConfirm(const QString &question);

    /*!
     * \brief flush Metoda provede vyprázndnění výstupního bufferu, což vynutí výpis dat do konzole zařízení.
     */
    static void flush();

private:
    /*!
     * \brief Enum UserAnswers
     *
     * Enum, který obsahuje možné odpovědi uživatele.
     */
    enum UserAnswers {
        Agreement    = 'y',         //!< Znak, který je od uživatele chápán jako souhlas.
        Disagreement = 'n'          //!< Znak, který je od uživatele chápán jakon nesouhlas.
    };
};

#endif
