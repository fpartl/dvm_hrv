#ifndef CONFIG_H
#define CONFIG_H

/* Datový typ jednoho vzorku EKG signálu. */
using ekg_sample = double;

/* Popis USB portu, ze kterého se budou číst data. */
#define USB_PORT "Dev1/ai0"

/* Užitá vzorkovací frekvence. */
#define FREQUENCY 1000

/*
 * Velikost bufferu (násobnky konstanty SAMPLE_RATE) pro nahrávání EKG signálu (užitý mezi
 * zápisy do výstupního souboru).
*/
#define BUFFER_SIZE 200


/*
 * Příznak, který říká, zda mají být data ve výstupním CSV souboru serializována v řádce, nebo má být
 * soubor rozdělen do řádek po sekundách
 */
#define WRITE_EKG_IN_ONE_ROW true

/* Schéma automaticky generovaného názvu výstupního souboru s EKG signálem. */
#define OUT_EKG_FILE_NAME "yyyy-MM-dd_hh-mm-ss_ekg"


/* Příkaz, který v prostředí operačního systému spustí interpret jazyk Python. */
#define PYTHON_EXECUTABLE "python"


//////////////////////////////////////////////////////////////////////////////////////////////////
/// Tyto konfiguráční konstanty jsou běžnému uživateli k ničemu a není doporučeno na ně sahat. ///
//////////////////////////////////////////////////////////////////////////////////////////////////

/* Definice smart pointeru, který přenášen mezi vlákny. */
#include <QVector>
#include <QSharedPointer>

/* Definice datového typu sdíleného ukazatele na vektor EKG vzorků. */
using EKGChunkPointer = QSharedPointer<QVector<ekg_sample>>;

/* Tady je volba mezi USBWorker a USBWorkerSimulator pro simulaci. */
#define INPUT_DEVICE USBWorker

#endif
