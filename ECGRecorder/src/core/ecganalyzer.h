﻿#ifndef ECGANALYZER_H
#define ECGANALYZER_H

#include <QObject>
#include <QStringList>
#include <QScopedPointer>
#include <QFileInfo>

#include "config.h"
#include "ecg/ecgloader.h"
#include "output/ekgoutfile.h"
#include "python/pythonrunner.h"
#include "libfp/console.h"

/*!
 * \brief Třída EkgAnalyzer
 *
 * Obsahuje objekty tříd pro záznam EKG signálu a spuštění pythnonovských skriptů nad pořízenými daty.
 */
class ECGAnalyzer : public QObject {
    Q_OBJECT

public:
    /*!
     * \brief EkgAnalyzer Konstruktor třídy.
     * \param outFile Ukazatel na objekt, který představuje výstupní soubor.
     * \param duration Délka záznamu v sekundách.
     * \param pythonScript Skript, který bude automaticky spuštěn s parametrem souboru s EKG daty.
     * \param pythonParams Dodatečné parametry pythonovského skriptu.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění dynamicky alokovaného
     *               objektu.
     */
    explicit ECGAnalyzer(EkgOutFile * const outFile, int duration, QString pythonScript, QStringList pythonParams, QObject *parent = nullptr);

    /*!
     * \brief loaderReady Metoda zkontroluje, zda je nahrávání EKG signálu připraveno k použití.
     * \return True, pokud je nahrávání EKG signálu připraveno, jinak false.
     */
    bool loaderReady();

    /*!
     * \brief pythonReady Metoda zkontroluje, zda je Python na lokálním zařízení připraven k použití.
     * \return True, pokud je Python připraven, jinak false.
     */
    bool pythonReady();

    /*!
     * \brief pythonScriptReady Metoda zkontroluje, zda je zadaný pythonovský skript připravený k použití.
     * \return True, pokud je skript připravený k použití, jinak false.
     */
    bool pythonScriptReady();

    /*!
     * \brief run Metoda spustí pracovní vlákna pro záznam EKG signálu do výstupního souboru.
     *            Ihned po dokončení vláken nahrávajíích EKG signál se zavolá metoda HrvAnalyzer::
     *            runPythonScript(), která spustí další analýzu.
     * \return True, pokud byla vlákna úspěšně spuštěna, jinak false.
     */
    bool run();

private:
    QScopedPointer<ECGLoader> m_loader;     //!< Objekt pro nahrávání EKG signálu z převodníku do souboru.
    EkgOutFile *m_outFile;                  //!< Ukazatel na objekt, který udržuje výstupní soubory.
    QString m_pythonScript;                 //!< Pythonovský skript, který bude spuštěn nad pořízenými vzorky EKG signálu.
    QStringList m_pythonParams;             //!< Dodatečné parametry pythonovského skriptu.
    PythonRunner m_pythonRunner;            //!< Objekt, skrze který jsou spouštěny pythonovské skripty.

    /*!
     * \brief runPythonScript Metoda spustí pythonovský skript m_pythonRunner.
     */
    void runPythonScript();

signals:
    /*!
     * \brief analysisFinished Signál, který informuje o tom, že byl záznam úspěšně dokončen a případný pytnohonovský
     *                         skript také doběhl.
     */
    void analysisFinished();
};

#endif
