#include "ecganalyzer.h"

ECGAnalyzer::ECGAnalyzer(EkgOutFile * const outFile, int duration, QString pythonScript, QStringList pythonParams, QObject *parent) : QObject(parent) {
    m_outFile = outFile;
    m_pythonScript = QFileInfo(pythonScript).absoluteFilePath();
    m_pythonParams = pythonParams;

    if (m_outFile) {
        m_loader.reset(new ECGLoader(USB_PORT, FREQUENCY, duration, m_outFile->dataOutFile(), this));

        QObject::connect(m_loader.data(), &ECGLoader::ekgLoadingFinished, this, &ECGAnalyzer::runPythonScript);
    }
}

bool ECGAnalyzer::loaderReady() {
    return !m_loader.isNull() && m_loader->isReady();
}

bool ECGAnalyzer::pythonReady() {
    return m_pythonRunner.isReady() /*&& m_pythonRunner.hasModules({ REQUIRED_MODULES })*/;
}

bool ECGAnalyzer::pythonScriptReady() {
    return PythonRunner::scriptExists(m_pythonScript);
}

bool ECGAnalyzer::run() {
    /* Spuštění pythonovského skriptu se děje automaticky pomocí signal-slot propojení. */
    return m_loader->start();
}

void ECGAnalyzer::runPythonScript() {
    if (pythonScriptReady()) {
        QStringList params;
        params << m_outFile->dataOutFile()->absFilePath();
        params << m_pythonParams;

        m_pythonRunner.runScript(m_pythonScript, params);
    }

    emit analysisFinished();
}
