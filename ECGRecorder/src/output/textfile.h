#ifndef TEXTFILE_H
#define TEXTFILE_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QIODevice>
#include <QTextStream>
#include <QDir>
#include <QSharedPointer>

/*!
 * \brief Třída TextFile
 *
 * Třída představuje obecný textový soubor.
 */
class TextFile : public QObject {

public:
    /*!
     * \brief TextFile Konstruktor třídy.
     * \param filePath Cesta ke spravovanému souboru.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     */
    explicit TextFile(QString filePath, QObject *parent = nullptr);

    /*!
     * \brief suffix Metoda vrátí příponu spravovaného souboru. Pokud soubor neexistuje, vrátí prázdný řetězec.
     * \return Přípona spravovaného souboru, nebo prázdný řetězec, pokud soubor neexistuje.
     */
    QString suffix() const;

    /*!
     * \brief create Metoda fyzicky vytvoří spravovaný souboru na uložišti zařízení. Pokud soubor již existuje,
     *               nebude provedena žádná další akce.
     * \return True, pokud byl soubor úspěšně vytvořen, jinak false.
     */
    bool create() const;

    /*!
     * \brief remove Metoda odstraní spravovaný soubor. Pokud soubor neexistuje, metoda neprovede žádné akce.
     */
    void remove();

    /*!
     * \brief exists Metoda zkontroluje, zda je spravovaný souboru dostupný na uložišti zařízení.
     *               Pokud je parametr writable = true, metoda rovněž kontroluje, zda je soubor
     *               zapisovatelný.
     * \return True, pokud je soubor fyzicky na disku zařízení připravený ke čtení, resp. zápisu, jinak false.
     */
    bool exists(bool writable = true) const;

    /*!
     * \brief clear Metoda vyprázdní spravovaný textový soubor. Pokud soubor neexistuje, neprovede žádnou akci.
     */
    void clear();

    /*!
     * \brief size Metoda vrátí velikost souboru.
     * \return Velikost spravovaného souboru.
     */
    qint64 size() const;

    /*!
     * \brief write Metoda do spravovaného textového souboru zapíše zadaný řetězec znaků. Pokud soubor neexistuje,
     *              metoda se jej pokusí vytvořit pomocí metody TextFile::create. Pokud soubor existuje, text bude
     *              zapsána na konec souboru.
     * \param text Řetězec znaků, který bude zapsán do spravovaného souboru.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool write(const QString &text) const;

    /*!
     * \brief writeLine Metoda do spravovaného souboru zapíše jednu řádku textu.
     * \param line Řádka textu, který bude zapsána do spravovaného souboru.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool writeLine(const QString &line) const;

    /*!
     * \brief readAll Metoda přečte veškerý obsah textového souboru, který vrátí jako objekt třídy QString. Pokud soubor
     *                neexistuje nebo je prázdný, metoda vrátí prázdný řetězec.
     * \return Obsah textového souboru nebo prázdný řetězec, pokud soubor neexistuje.
     */
    QString readAll() const;

    /*!
     * \brief filePath Metoda vrátí cestu k souboru, která byla zadána v konstruktoru třídy.
     * \return Cesta k souboru, která byla zadána v konstruktoru třídy.
     */
    QString filePath() const;

    /*!
     * \brief absFilePath Metoda vrátí absolutní cestu k souboru, který byl zadán cestou v konstruktoru třídy.
     * \return Absolutní umístění souboru, který byl zadán v konstruktoru třídy.
     */
    QString absFilePath() const;

    /*!
     * \brief isValidFileName Metoda zkontroluje, zda zadaná cesta může ukazovat na soubor. Tedy pokud adresář, kam cesta
     *                        ukazuje skutečně existuje a soubor má platnou příponu.
     * \param fileName Kontrolovaná cesta.
     * \return True, pokud umístění může být bráno jako umístění nového souboru.
     */
    static bool isValidFileName(const QString &fileName);

private:
    QString m_filePath;     //!< Umístění spravovaného souboru.

protected:
    /*!
     * \brief filePtr Metoda vrátí ukazatel na deskriptor souboru, u kterého je zaručeno, že je
     *                soubor otevření v řežimu QIODevice::ReadWrite. V případě chyby je vráce null pointer.
     * \return Ukazatel na deskriptor souboru, nebo nullpointer v případě chyby.
     */
    QSharedPointer<QFile> filePtr() const;
};

#endif
