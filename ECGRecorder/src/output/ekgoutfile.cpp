#include "ekgoutfile.h"

EkgOutFile::EkgOutFile(QString userInput, QObject *parent) : QObject(parent) {
    if (userInput.isEmpty())
        return;

    initFile(userInput);
}

bool EkgOutFile::isReady() const {
    return !m_dataOutFile.isNull() && m_dataOutFile->exists();
}

QString EkgOutFile::dataOutFilePath() const {
    return isReady() ? m_dataOutFile->filePath() : "";;
}

CSVFile<ekg_sample> *EkgOutFile::dataOutFile() const {
    return isReady() ? m_dataOutFile.data() : nullptr;
}

void EkgOutFile::initFile(QString &userInput) {
    QString dataOutFile;

    appendDirSep(&userInput);

    if (isFolder(userInput)) {
        QString outName = QDateTime::currentDateTime().toString(OUT_EKG_FILE_NAME);

        dataOutFile = QString("%1%2.%3").arg(userInput).arg(outName)
                            .arg(CSVFile<ekg_sample>::fileSuffix());
    }
    else if (CSVFile<ekg_sample>::isValidFileName(userInput)) {
        dataOutFile = userInput;
    }
    else return;

    if (askForRewrite(dataOutFile)) {
        m_dataOutFile.reset(new CSVFile<ekg_sample>(dataOutFile));
        m_dataOutFile->create();
    }
}

void EkgOutFile::appendDirSep(QString * const userInput) {
    if (!userInput || !QFileInfo(*userInput).isDir())
        return;

    if (userInput->back() != QDir::separator())
        userInput->append(QDir::separator());
}

bool EkgOutFile::isFolder(const QString &filePath) const {
    QFileInfo info(filePath);

    return info.exists() && info.isDir() && info.isWritable();
}

bool EkgOutFile::askForRewrite(const QString &filePath) const {
    if (!QFileInfo(filePath).exists())
        return true;

    return Console::printConfirm(QString("Skutečně chcete přepsat soubor %1?").arg(filePath));
}
