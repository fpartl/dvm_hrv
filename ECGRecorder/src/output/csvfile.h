#ifndef CSVFILE_H
#define CSVFILE_H

#include <QObject>
#include <QVector>
#include <QVectorIterator>
#include <QString>
#include <QSharedPointer>
#include <QTextStream>

#include "textfile.h"

/* Přípona souboru typu csv. */
#define CSV_FILE_SUFFIX "csv"

/* Oddělovač hodnot. */
#define SAMPLE_SEPARATOR ";"

/* Oddělovač řádků dle specifikace CSV (ne podle platformy!). */
#define LINE_SEPARATOR "\r\n"

template <class T>
/*!
 * \brief Třída CSVFile
 *
 * Třída představuje CSV souboru, u kterého je možné zapisovat a číst hodnoty typu T. Typ T musí mít implementovanou
 * metodu toString, aby vše fungovalo dle očekávání.
 */
class CSVFile : public TextFile {

public:
    /*!
     * \brief CSVFile Konstruktor třídy.
     * \param fileName Cesta ke spravovanému csv souboru.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     */
    explicit CSVFile(QString fileName, QObject *parent = nullptr);

    /*!
     * \brief exists Metoda pomocí metody TextFile::exists kontroluje existenci souboru a navíc
     *               kontroluje platnost přípony.
     * \return True, pokud csv soubor existuje, jinak false.
     */
    bool exists(bool writable = true) const;

    /*!
     * \brief write Metoda na konec spravovaného souboru zapíše množinu zadaných hodnot oddělených
     *              znakem SAMPLE_SEPARATOR. Pokud bude vektor vstupních hodnot prázdný, metoda neprovede
     *              žádnou akci a vrátí hodnotu true.
     * \param data Vkládaná data.
     * \return True, pokud byla zápis úspěšný, jinak false.
     */
    bool write(const QVector<T> &data) const;

    /*!
     * \brief writeLine Metoda na konec spravovaného souboru zapíše novou řádku, která obsahuje zadané
     *                  hodnoty oddělené znakem SAMPLE_SEPARATOR. Pokud bude vektor vstupních hodnot prázdný
     *                  metoda neprovede žádnou akci a vrátí true.
     * \param data Vkládaná data.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool writeLine(const QVector<T> &data) const;

    /*!
     * \brief readAll Metoda načte všechny hodnoty ze spravovaného CSV souboru. Pokud soubor neexistuje nebo
     *                se hodnoty nepodařilo převést na typ T, metoda vrátí prázdný vektor.
     * \return Vektor načtených hodnot, nebo prázdný vektor při chybě.
     */
    QVector<T> readAll() const;

    /*!
     * \brief isValidFileName Metoda zkontroluje, zda zadaná cesta může ukazovat na soubor. Tedy pokud adresář, kam cesta
     *                        ukazuje skutečně existuje a soubor má platnou příponu.
     * \param fileName Kontrolovaná cesta.
     * \return True, pokud umístění může být bráno jako umístění nového souboru.
     */
    static bool isValidFileName(const QString &fileName);

    /*!
     * \brief fileSuffix Metoda vrátí platnou příponu souboru typu CSV.
     * \return Platná přípona souboru typu CSV.
     */
    static QString fileSuffix();

private:
    /*!
     * \brief joinText Metoda konkatenuje všechny položky vektoru data do jediného řetězce znaků, jehož hodnoty
     *                 oddělí řetězcem SAMPLE_SEPARATOR. Pokud bude parametr prependSeparator nastaven na true,
     *                 separátor bude umístěn i na začátek řetězce. Pokud bude vstupní vektor prázdný, metoda
     *                 vrátí prázdný řetězec.
     * \param data Vektor dat, která budeou seializována do výstupního řetězce.
     * \param prependSeparator Příznak, zda má být řetězec SAMPLE_SEPARATOR umístěn i na začátek výstupního řetězce.
     * \return Řetězec znaků, který obsahuje položky vstupního vektoru oddělé znakem SAMPLE_SEPARATOR.
     */
    QString joinText(const QVector<T> &data, bool prependSeparator = false) const;
};

template<class T>
CSVFile<T>::CSVFile(QString fileName, QObject *parent) : TextFile(fileName, parent) { }

template<class T>
bool CSVFile<T>::exists(bool writable) const {
    return TextFile::exists(writable)
            && TextFile::suffix().compare(CSV_FILE_SUFFIX) == 0;
}

template<class T>
bool CSVFile<T>::write(const QVector<T> &data) const {
    QString toWrite = joinText(data, TextFile::size() > 0);
    if (toWrite.isEmpty())
        return true;

    return TextFile::write(toWrite);
}

template<class T>
bool CSVFile<T>::writeLine(const QVector<T> &data) const {
    QString toWrite = joinText(data, false);
    toWrite.append(LINE_SEPARATOR);

    return TextFile::write(toWrite);
}

template<class T>
QVector<T> CSVFile<T>::readAll() const {
    QSharedPointer<QFile> filePtr = TextFile::filePtr();
    if (filePtr.isNull())
        return QVector<T>();

    QVector<T> values;

    QTextStream stream(filePtr.data());
    while (!stream.atEnd()) {
        QString line = stream.readLine();

        QStringList samples = line.split(SAMPLE_SEPARATOR);
        for (QString sample : samples) {
            //TODO: Tady je bota jako bejk... tohle se chci naučit vyřešit.
            bool isDouble;
            double value = sample.toDouble(&isDouble);

            if (isDouble)
                values.append(value);
        }
    }

    filePtr->close();
    return values;
}

template<class T>
bool CSVFile<T>::isValidFileName(const QString &fileName) {
    return TextFile::isValidFileName(fileName) && QFileInfo(fileName).suffix().compare(CSV_FILE_SUFFIX) == 0;
}

template<class T>
QString CSVFile<T>::fileSuffix() {
    return CSV_FILE_SUFFIX;
}

template<class T>
QString CSVFile<T>::joinText(const QVector<T> &data, bool prependSeparator) const {
    if (data.isEmpty())
        return QString();

    QString joined;
    if (prependSeparator)
        joined.append(SAMPLE_SEPARATOR);

    QVectorIterator<T> i(data);
    while (i.hasNext()) {
        joined.append(QString("%1").arg(i.next()));

        if (i.hasNext())
            joined.append(SAMPLE_SEPARATOR);
    }

    return joined;
}



#endif
