#include "textfile.h"

TextFile::TextFile(QString filePath, QObject *parent) : QObject(parent) {
    m_filePath = filePath;
}

QString TextFile::suffix() const {
    if (!exists(false))
        return QString();

    return QFileInfo(m_filePath).suffix();
}

bool TextFile::create() const {
    bool created;
    QFile newFile(m_filePath);

    created = newFile.open(QIODevice::WriteOnly);
    if (created)
        newFile.close();

    return created;
}

void TextFile::remove() {
    if (!exists())
        return;

    QFile poor(m_filePath);
    poor.remove();
}

bool TextFile::exists(bool writable) const {
    QFileInfo info(m_filePath);

    bool exists = info.exists() && info.isReadable() && info.isFile();
    if (writable)
        exists = exists && info.isWritable();

    return exists;
}

void TextFile::clear() {
    if (!exists())
        return;

    /* Provádí prakticky stejnou, tj. požadovou činnost. */
    create();
}

qint64 TextFile::size() const {
    return QFileInfo(m_filePath).size();
}

bool TextFile::write(const QString &text) const {
    if (text.isEmpty() || !exists())
        return false;

    QFile file(m_filePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Append))
        return false;

    QTextStream(&file) << text;
    file.close();
    return true;
}

bool TextFile::writeLine(const QString &line) const {
    if (line.isEmpty() || !exists())
        return false;

    QFile file(m_filePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Append))
        return false;

    QTextStream(&file) << line << endl;
    file.close();
    return true;
}

QString TextFile::readAll() const {
    if (!exists(false))
        return QString();

    QFile file(m_filePath);
    if (!file.open(QIODevice::ReadOnly))
        return QString();

    QString content = QTextStream(&file).readAll();

    file.close();
    return content;
}

QString TextFile::filePath() const {
    return m_filePath;
}

QString TextFile::absFilePath() const {
    return QFileInfo(m_filePath).absoluteFilePath();
}


bool TextFile::isValidFileName(const QString &fileName) {
    if (fileName.isEmpty())
        return false;

    QFileInfo fileInfo(fileName);
    QFileInfo dirInfo(fileInfo.path());

    return dirInfo.exists() && dirInfo.isDir() && !fileInfo.isDir() && dirInfo.isWritable();
}

QSharedPointer<QFile> TextFile::filePtr() const {
    if (!exists(true))
        return QSharedPointer<QFile>();

    QSharedPointer<QFile> ptr = QSharedPointer<QFile>::create(m_filePath);

    return ptr->open(QIODevice::ReadWrite) ? ptr : QSharedPointer<QFile>();
}
