#ifndef EKGOUTFILE_H
#define EKGOUTFILE_H

#include <QObject>
#include <QString>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>
#include <QScopedPointer>

#include "libfp/console.h"
#include "output/csvfile.h"
#include "config.h"

/*!
 * \brief Třída HrvOutFiles
 *
 * Třída reprezentuje výstupní soubor aplikace, tj. soubor se záznamem EKG signálu. Inicializace
 * objektu této třídy vyžaduje od uživatele vstup v podobě cesty ke konkrétnímu souboru, kam se
 * budou vzorky zapisovat nebo umístění adresáře, kde budou soubory automaticky vytvořeny s názvem
 */
class EkgOutFile : public QObject {
    Q_OBJECT

public:
    /*!
     * \brief HrvOutFiles Konstruktor třídy.
     * \param userInput Vstup od uživatele, který představuje buď cestu přímo k výstupnímu
     *                  souboru nebo adresář, kde bude výstupní soubor vytvořen.
     * \param parent Ukazatel na rodiče objektu, který podle notace Qt lze využít pro uvolnění
     *               dynamicky alokovaného objektu.
     */
    explicit EkgOutFile(QString userInput, QObject *parent = nullptr);

    /*!
     * \brief isReady Metoda zkontroluje, zda je výstupní souboru připraven na příjem nových dat.
     * \return True, pokud je možné do výstupního souboru zapisovat nová data, jinak false.
     */
    bool isReady() const;

    /*!
     * \brief dataOutFile Metoda, která vrátí umístění souboru se vzorky naměřeného EKG signálu. Pokud
     *                    soubor není připraven, tj. metoda HrvOutFiles::isReady() vrátí hodnotu false,
     *                    tato metoda vrátí prázdný řetězec.
     * \return Cesta v výstupnímu souboru s naměřenými vzorky EKG signálu, nebo prázdný řetězec, pokud
     *         soubor není připraven.
     */
    QString dataOutFilePath() const;

    /*!
     * \brief dataOutFile Metoda vrutí ukazatel na výstupní souboru se zaznamenanými vzorky EKG signálu. Uvolnění
     *                    vráceného ukazatele vedek nedefinovanému chování. Pokud HrvOutFiles::isReady vrátí false,
     *                    metoda vrací nullptr.
     * \return Ukazatel na výstupní soubor se zaznamenanými vzorky EKG signálu nebo nullptr pokud HrvOutFiles::isReady
     *         vrátí false.
     */
    CSVFile<ekg_sample> *dataOutFile() const;

private:
    QScopedPointer<CSVFile<ekg_sample>> m_dataOutFile;      //!< Ukazatel na souboru s pořízenými vzorky EKG signálu.

    /*!
     * \brief initFile Metoda podle zadání uživatele provede inicializaci atributu m_dataOutFile.
     * \param userInput Vstup od uživatele (adresář nebo konkrétní soubor).
     */
    void initFile(QString &userInput);

    /*!
     * \brief appendDirSep Metoda ze zadaného vstupu od uživatele přidá znak oddělující adresáře
     *                     z konce řetězce (pokud se zde samozřejmě již nachází).
     * \param userInput Vstup od uživatele.
     */
    void appendDirSep(QString * const userInput);

    /*!
     * \brief isFolder Metoda zkontroluje, zda zadaná cesta představuje umístění existujícího adresáře,
     *                 do kterého je možné zapisovat data.
     * \param filePath Cesta, která bude kontrolovanána.
     * \return True, pokud cesta představuje umístění užitelného adresáře, jinak false.
     */
    bool isFolder(const QString &filePath) const;

    /*!
     * \brief askForRewrite Metoda se uživatele dotáže, zda existující soubor může přepsat. Návratová
     *                      hodnota metody je pak analogická s jeho uživatelovou odpovědí.
     * \param filePath Cesta k souboru, který je třeba přepsat.
     * \return True, pokud uživatel souhlasí s přepsáním souboru, jinak false.
     */
    bool askForRewrite(const QString &filePath) const;
};
#endif
