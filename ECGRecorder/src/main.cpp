#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QStringListIterator>
#include <limits>

#include "libfp/console.h"
#include "output/ekgoutfile.h"
#include "core/ecganalyzer.h"

/* Formát spuštěcího příkazu. Pořadí vstupních parametrů je pevně dáno! */
#define USAGE_MESSAGE "Usage: %1 <vystupni-adresar-nebo-soubor-csv> <delka-sledovani-v-minutach> [spustitelny-pythonovsky-skript]"

/*
 * Použití aplikace:
 *
 * OS Linux:
 *      ./<sputitelny-soubor> <adresář-nebo-výstupní soubor> <délka-sledování-v-minutách> [pythonovsky-skript]
 *
 * OS Windows
 *      <sputitelny-soubor>.exe <adresář-nebo-výstupní-soubor> <délka-sledování-v-minutách> [pythonovsky-skript]
 *
 * Pokud zadáte cestu k existujícímu adresáři, tak aplikace sama vygeneruje název výstupního
 * souboru ve formátu "OUT_EKG_FILE_NAME.csv", který obsahuje přímo vzorky naměřeného EKG signálu
 * oddělené středníkem. Je také možné zadat přímo název souboru, který bude po stvrzení uživatelem
 * přepsán.
 */

/*!
 * \brief main Hlavní přístupový bod aplikace.
 * \param argc Počet vstupních parametrů (včetně názvu spuštěného binárního souboru).
 * \param argv Vstupní parametry (včetně názvu spuštěného binárního souboru).
 * \return EXIT_SUCCESS, pokud vykonávání programu dopadne úspěšně, jinak EXIT_FAILURE.
 */
int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    if (argc < 3) {
        Console::writeLine(QString(USAGE_MESSAGE).arg(argv[0]));
        return EXIT_FAILURE;
    }

    EkgOutFile outFile(argv[1]);
    if (!outFile.isReady()) {
        Console::writeLine("Nepodarilo se vytvorit vystupni soubor. Mozna jste zadali neplatne umisteni.");
        Console::writeLine(QString(USAGE_MESSAGE).arg(argv[0]));
        return EXIT_FAILURE;
    }

    bool isNumeric;
    int duration = QString(argv[2]).toInt(&isNumeric);
    if (!isNumeric || duration <= 0) {
        Console::writeLine(QString("Zadana delka mereni je neplatna. Volte dobu z intervalu <1, %1>.").arg(std::numeric_limits<int>::max()));
        Console::writeLine(QString(USAGE_MESSAGE).arg(argv[0]));
        return EXIT_FAILURE;
    }

    QString pythonScript = argc > 3 ? QString(argv[3]) : "";

    QStringList pythonParams;
    for (int i = 4; i < argc; i++)
        pythonParams << QString(argv[i]);

    ECGAnalyzer analyzer(&outFile, duration, pythonScript, pythonParams);
    QObject::connect(&analyzer, &ECGAnalyzer::analysisFinished, &a, &QCoreApplication::quit);

    if (!analyzer.loaderReady()) {
        Console::writeLine("Napodarilo se inicializovat nahravani EKG signalu. Máte nainstalovane ovladace A/D prevodniku dle dokumentace?");
        Console::writeLine(QString(USAGE_MESSAGE).arg(argv[0]));
        return EXIT_FAILURE;
    }

    if (!pythonScript.isEmpty()) {
        if (!analyzer.pythonReady()) {
            Console::writeLine("Pro spusteni pythonovskeho skriptu je treba mit nainstalovany Python verze minimálne 3.");
            return EXIT_FAILURE;
        }

        if (!analyzer.pythonScriptReady()) {
            Console::writeLine(QString("Pythonovsky skript \"%1\" se nepodarilo nalezt...").arg(pythonScript));
            return EXIT_FAILURE;
        }
    }

    if (!analyzer.run()) {
        Console::writeLine("Oou... neco se pokazilo (kontaktujte me na fpartl@students.zcu.cz)!");
        return EXIT_FAILURE;
    }

    return a.exec();
}
