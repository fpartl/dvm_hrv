from rr import load_data, get_rr_intervals, interpolate_nans, drop_nans
import numpy as np
import matplotlib.pyplot as plt

import hrv_count

# settings
ignore_wrong_areas = False

signal = load_data(file_path="simulation.txt", val_delimiter="\t", drop_first_col=True)
# signal[97000:105000] = signal[0:8000]
signal[110000:114000] = 0

# get normalized RR intervals with nans as wrong intervals
rr_intervals, r_peaks, wrong_areas, num_wrong_area_peaks = get_rr_intervals(data=signal, sample_rate=1000, ignore_wrong_areas=ignore_wrong_areas, plot=True)

# interpolate wrong RR peaks
interpolated_rr = interpolate_nans(rr_intervals, r_peaks)

# drop wrong RR intervals
dropped_rr = drop_nans(rr_intervals)


# user should know how much of intervals is real
num_rr = len(rr_intervals) - 1              # first interval is always nan
num_nan = sum(np.isnan(rr_intervals)) - 1   # first interval is always nan
sig_len = len(signal)
wrong_areas_len = np.sum([x[1] - x[0] for x in wrong_areas])

if ignore_wrong_areas:
    print("wrong areas (ignored): " + str(100*wrong_areas_len/sig_len) + " % of signal")
    print("artifacts: " + str(
        100 * (num_nan - num_wrong_area_peaks) / (num_rr - num_wrong_area_peaks)) + " %")
else:
    print("wrong areas: " + str(100*wrong_areas_len/sig_len) + " % of signal")
    print("artifacts: " + str(
        100 * (num_nan - num_wrong_area_peaks) / (num_rr - num_wrong_area_peaks)) + " % (outside wrong areas)")
print("wrong values: " + str(100*num_nan/num_rr) + " % (total wrong peaks including artifacts and peaks in wrong areas)")


# plotting
plt.figure("RR intervals")
plt.subplot(3, 1, 1)
plt.plot(signal, linewidth=1)
plt.plot(r_peaks, signal[r_peaks], "x", color="green")
nan_indexes = []
for irr, rr in enumerate(rr_intervals):
    if np.isnan(rr):
        nan_indexes.append(irr)
plt.plot(r_peaks[nan_indexes], signal[r_peaks[nan_indexes]], "x", color="red")
for interval in wrong_areas:
    plt.axvspan(interval[0], interval[1], color='red', alpha=0.5)
plt.title("R peaks for RR intervals (time before peak).")
plt.legend(["ECG signal", "ok R peak", "wrong R peak", "wrong area"])


plt.subplot(3, 1, 2)
plt.plot(signal, linewidth=2)
rrx = np.cumsum(interpolated_rr) + r_peaks[0]
for rri in rrx:
    plt.axvline(rri, linewidth=1, color='red')
plt.title("Interpolated RR")
plt.legend(["ECG signal", "intervals"])

plt.subplot(3, 1, 3)
plt.plot(signal, linewidth=2)
rrx = np.cumsum(dropped_rr) + r_peaks[0]
for rri in rrx:
    plt.axvline(rri, linewidth=1, color='red')
plt.title("Dropped RR")
plt.legend(["ECG signal", "intervals"])

hrv_count.run(interpolated_rr)

plt.show()
