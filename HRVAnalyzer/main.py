from rr import load_data, get_rr_intervals, drop_nans
import numpy as np
import sys
import hrv_count


def process_to_rr(csv_path, sample_rate):
    # loading data
    signal = load_data(file_path=csv_path, val_delimiter=";")
    assert len(signal) > 0, "Signal could not be loaded."

    # get normalized RR intervals with nans as wrong intervals
    rr_intervals, r_peaks, wrong_areas, num_wrong_area_peaks = get_rr_intervals(data=signal, sample_rate=sample_rate,
                                                                                ignore_wrong_areas=False, plot=False)
    assert len(r_peaks) > 0, "No R peaks found."

    # drop wrong RR intervals
    rr = drop_nans(rr_intervals)
    assert len(rr) > 0, "No valid RR intervals obtained."

    # user should know how much of intervals is real
    num_rr = len(rr_intervals) - 1  # first interval is always nan
    num_nan = sum(np.isnan(rr_intervals)) - 1  # first interval is always nan
    sig_len = len(signal)
    wrong_areas_len = np.sum([x[1] - x[0] for x in wrong_areas])
    print("signal informations:")
    print("\twrong areas: " + str(100 * wrong_areas_len / sig_len) + " % of signal")
    print("\tartifacts: " + str(
        100 * (num_nan - num_wrong_area_peaks) / max((num_rr - num_wrong_area_peaks), 1)) + " % (wrong R peaks outside wrong areas)")
    print("\twrong values: " + str(
        100 * num_nan / num_rr) + " % (total wrong peaks including artifacts and peaks in wrong areas)")

    return rr


assert len(sys.argv) == 2

rr = process_to_rr(csv_path=sys.argv[1], sample_rate=1000)

hrv_count.run(rr)