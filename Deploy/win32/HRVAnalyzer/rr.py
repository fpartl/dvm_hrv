__author__ = 'Josef Baloun'


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import pandas as pd


def load_data(file_path, val_delimiter, drop_first_col=False):
    """
    Loading signal.

    :param file_path: Path to file with ECG data (could be table of values).
    :param val_delimiter: Values delimiter in file.
    :param drop_first_col: If True, then the first column in file is not taken as values.
    :return: loaded 1D data
    """
    data = np.genfromtxt(file_path, delimiter=val_delimiter, dtype=float)
    assert data.size > 1, "Not enough data to load."

    if drop_first_col:
        data = data[:, 1:]

    if data.size == len(data):     # 1D input
        if np.isnan(data[-1]):      # could occur if delimiter is before end
            data = data[:-1]
    else:                           # 2D input
        if np.isnan(data[0, -1]):   # could occur if delimiter is before \n, then the last column is nan
            data = data[:, :-1]
    data = data.flatten()
    return data


def get_rr_intervals(data, sample_rate, min_bpm=30, max_bpm=240, qrs_time=0.1, ignore_wrong_areas=False, plot=False):
    """
    Loading signal.

    :param data: Signal.
    :param sample_rate: Sample rate of signal.
    :param min_bpm: Minimal beats per minute to detect.
    :param max_bpm: Maximal beats per minute to detect.
    :param qrs_time: Normal time of QRS complex in seconds.

    :return: Normal to normal RR intervals in miliseconds (first is nan), R peaks detected, wrong area intervals, number of nan peaks because of wrong areas (excl. first peak)
    """
    rr = RR(data=data, sample_rate=sample_rate, min_bpm=min_bpm, max_bpm=max_bpm, qrs_time=qrs_time)

    qrs_peaks, smooth_squared_diff = rr.findQRSPeaks(plot=plot)

    qrs_peaks, wrong_areas = rr.removeQRSPeaksOutliers(qrs_peaks, smooth_squared_diff, plot=plot)
    smooth_squared_diff = None  # not needed anymore

    r_peaks = rr.findRPeaks(qrs_peaks, plot=plot)

    nn_intervals, num_wrong_area_peaks = rr.getNNintervals(r_peaks, wrong_areas, ignore_wrong_areas=ignore_wrong_areas, plot=plot)

    return nn_intervals, r_peaks, wrong_areas, num_wrong_area_peaks


def interpolate_nans(rr_intervals, r_peaks):
    """
    Interpolating nan values in rr_intervals.

    :param rr_intervals: RR intervals with nans.
    :param r_peaks: R peaks according to rr_intervals.
    :return: Interpolated values. Len of array could be different to rr_intervals.
    """
    rr = _interpolate_nans(rr_intervals, r_peaks)
    rr = _interpolate_nan_values(rr)
    return rr


def drop_nans(rr_intervals):
    """
    :param rr_intervals: RR intervals with nans.
    :return: RR intervals without nans.
    """
    series = pd.Series(rr_intervals)
    x = series.dropna()
    return x.values


class RR(object):
    """
    Class for RR interval detection.
    """

    def __init__(self, data, sample_rate, min_bpm=30, max_bpm=240, qrs_time=0.1):
        """
        Initialization.

        :param data: Signal.
        :param sample_rate: Sample rate of signal.
        :param min_bpm: Minimal beats per minute to detect.
        :param max_bpm: Maximal beats per minute to detect.
        :param qrs_time: Normal time of QRS complex in seconds.
        """
        assert len(data) == data.size, "Not 1D data."
        assert len(data) >= sample_rate, "At least one second of signal is required."
        assert not np.isnan(data).any(), "Input signal contains nan values."
        self.signal = data
        self.sample_rate = sample_rate
        self.min_bps = min_bpm / 60
        self.max_bps = max_bpm / 60
        self.qrs_time = qrs_time

    def findQRSPeaks(self, filter_order=4, low_cutoff=5, high_cutoff=15, plot=False):
        """
        Finds R peaks. It is based on Pan Tomkins algorithm for QRS detection.

        # Process:
        # 1) bandpass filter
        # 2) derivating filter to high light the QRS complex.
        # 3) Signal is squared.
        # 4) Signal is smoothed of noise (length of window depends on maximal beats per minute,
        #                       minimum length is set according to normal time of QRS complex).
        # 5) QRS peaks are retrieved depending on the settings.

        :param filter_order: The order of the digital bandpass Butterworth filter.
        :param low_cutoff: Low cutoff frequency for digital bandpass Butterworth filter.
        :param high_cutoff: High cutoff frequency for digital bandpass Butterworth filter.
        :param plot: If True, the process of retrieving QRS Peaks is plotted.
        :return: QRS peaks, smoothed squared derivation signal
        """
        # 1) bandpass filter
        sosfilter = sig.butter(N=filter_order, Wn=[low_cutoff, high_cutoff], btype="bandpass", analog=False,
                               output="sos", fs=self.sample_rate)
        smooth_squared_diff = sig.sosfiltfilt(sosfilter, self.signal)
        if plot:
            plt.figure("Finding QRS peaks")
            subplotnum = 5
            i = 1
            plt.subplot(subplotnum, 1, i)
            plt.plot(smooth_squared_diff, linewidth=1)
            plt.title("Filtered signal")
            i += 1

        # 2) derivating filter to high light the QRS complex.
        smooth_squared_diff = np.ediff1d(smooth_squared_diff, to_end=0)
        if plot:
            plt.subplot(subplotnum, 1, i)
            plt.plot(smooth_squared_diff, linewidth=1)
            plt.title("Derivation of filtered signal")
            i += 1

        # 3) Signal is squared. Reduction of T-wave.
        smooth_squared_diff = np.square(smooth_squared_diff)
        if plot:
            plt.subplot(subplotnum, 1, i)
            plt.plot(smooth_squared_diff, linewidth=1)
            plt.title("Squared derivation of filtered signal")
            i += 1

        # 4) Signal is smoothed of noise (length of window depends on maximal beats per minute: 1 / (2 * self.max_bps),
        #                       minimum length is set according to normal time of QRS complex).
        window_len = int(max(self.qrs_time, 1 / (2 * self.max_bps)) * self.sample_rate)
        window = sig.windows.hamming(window_len)
        window /= np.sum(window)    # averaging window
        smooth_squared_diff = np.convolve(smooth_squared_diff, window, mode="same")

        # 5) QRS peaks are retrieved depending on the settings.
        min_distance = int(self.sample_rate / self.max_bps)
        qrs_peaks, _ = sig.find_peaks(smooth_squared_diff, distance=min_distance)

        if plot:
            plt.subplot(subplotnum, 1, i)
            plt.plot(smooth_squared_diff, linewidth=1)
            plt.plot(qrs_peaks, smooth_squared_diff[qrs_peaks], "x", color="red")
            plt.title("QRS peaks in smoothed squared derivation.")
            i += 1

            plt.subplot(subplotnum, 1, i)
            plt.plot(self.signal, linewidth=1)
            plt.plot(qrs_peaks, self.signal[qrs_peaks], "x", color="red")
            plt.title("QRS peaks in original ECG signal")
            plt.show()

        return qrs_peaks, smooth_squared_diff

    def removeQRSPeaksOutliers(self, peaks, smooth_squared_diff, percentile=75, plot=False):
        """
        Removes wrong QRS peaks and detects wrong areas of signal.

        :param peaks: QRS peaks with outliers from findQRSPeaks function
        :param smooth_squared_diff: smoothed squared deviation of filtered signal from findQRSPeaks function
        :param percentile: percentile to use, when detecting wrong areas, higher percentile -> less wrong areas
        :param plot: If True, the process of removing QRS outliers is plotted.
        :return: QRS peaks coordinates, wrong areas
        """
        window = sig.windows.hamming(int(2 * self.sample_rate / self.min_bps))
        window = window / np.sum(window)
        # how is the signal changing in an area
        ssd_smooth = np.convolve(smooth_squared_diff, window, "same")

        # Detecting wrong QRS peaks
        qrs_peaks = self._filterLowQRSPeaks(peaks, smooth_squared_diff, ssd_smooth)

        if plot:
            plt.figure("Removing QRS outliers")
            subplotnum = 3
            i = 1
            plt.subplot(subplotnum, 1, i)
            plt.plot(smooth_squared_diff, linewidth=1)
            plt.plot(ssd_smooth, linewidth=1)
            plt.plot(peaks, smooth_squared_diff[peaks], "x", color="red")
            plt.plot(qrs_peaks, smooth_squared_diff[qrs_peaks], "x", color="green")
            plt.title("Filtering QRS peaks in smoothed squared derivation (SSD).")
            plt.legend(["SSD", "SSD smoothed", "wrong peak", "ok peak"])
            i += 1

        # Detecting wrong areas
        ssd_peaks, _ = sig.find_peaks(ssd_smooth)
        upper_bound, outlier_indexes = self._countUpperBound(values=ssd_smooth[ssd_peaks], percentile=percentile)
        wrong_areas = self._wrongAreas(ssd_peaks, outlier_indexes, len(ssd_smooth))

        if plot:
            plt.subplot(subplotnum, 1, i)
            plt.plot(ssd_smooth, linewidth=1)
            plt.plot([0, len(ssd_smooth)], [upper_bound, upper_bound], linewidth=1)
            plt.plot(ssd_peaks, ssd_smooth[ssd_peaks], "x", color="green")
            plt.plot(ssd_peaks[outlier_indexes], ssd_smooth[ssd_peaks[outlier_indexes]], "x", color="red")
            for interval in wrong_areas:
                plt.axvspan(interval[0], interval[1], color='red', alpha=0.5)
            plt.title("Detecting wrong areas")
            plt.legend(["SSD smoothed", "threshold", "ok peak", "wrong peak", "wrong area"])
            i += 1

            plt.subplot(subplotnum, 1, i)
            plt.plot(self.signal, linewidth=1)
            plt.plot(peaks, self.signal[peaks], "x", color="red")
            plt.plot(qrs_peaks, self.signal[qrs_peaks], "x", color="green")
            for interval in wrong_areas:
                plt.axvspan(interval[0], interval[1], color='red', alpha=0.5)
            plt.title("Result")
            plt.legend(["ECG signal", "wrong QRS", "ok QRS", "wrong area"])
            plt.show()

        return qrs_peaks, wrong_areas

    def findRPeaks(self, qrs_peaks, plot=False):
        """
        Finds maximum at QRS peak's surroundings.

        :param qrs_peaks: QRS peaks
        :param plot: If True, peaks are plotted.
        :return: corrected R peaks
        """
        # if QRS 0.1 s => looking for max at peak +- 0.05 s
        tolerance = int((self.qrs_time / 2) * self.sample_rate)
        r_peaks = []
        for p in qrs_peaks:
            low = max(0, p - tolerance)
            up = min(len(self.signal), p + tolerance)
            maxx = np.argmax(self.signal[low:up]) + low
            r_peaks.append(maxx)
        r_peaks = np.asarray(r_peaks)

        if plot:
            plt.figure("Finding R peaks")
            plt.plot(self.signal, linewidth=1)
            plt.plot(qrs_peaks, self.signal[qrs_peaks], "x", color="red")
            plt.plot(r_peaks, self.signal[r_peaks], "x", color="green")
            plt.title("R peaks correction")
            plt.legend(["ECG signal", "QRS peaks", "R peaks"])
            plt.show()

        return r_peaks

    def getNNintervals(self, r_peaks, wrong_areas, ignore_wrong_areas=False, plot=False):
        """
        Returns Normal to Normal R peak intervals. Wrong intervals are considered according to min/max bpm
        or if there is a difference 20 or more % according to malik rule.

        :param r_peaks: sorted R peak coordinates
        :param wrong_areas: wrong areas in signal - values will be ignored
        :param ignore_wrong_areas: If true, wrong areas are ignored and values are not ignored.
        :param plot: If True, peaks used for NN intervals are plotted.
        :return: NN intervals in ms (wrong are set to np.nan), number of nan peaks because of wrong area wrong areas (excl. first peak - is always nan)
        """
        rr_intervals = np.ediff1d(r_peaks, to_begin=0).astype(float)  # ary.flat[1:] - ary.flat[:-1]

        # wrong rr intervals are set to np.nan
        max_int = self.sample_rate / self.min_bps
        min_int = self.sample_rate / self.max_bps

        force_next = False
        last_interval = 0
        for i, rr in enumerate(rr_intervals):
            if force_next:
                last_interval = rr
                force_next = False
                continue

            if not min_int <= rr <= max_int:  # not in range of min / max bpm
                rr_intervals[i] = np.nan
                force_next = True
            elif abs(rr - last_interval) / rr > 0.2:    # malik rule - difference is bigger than 20 %
                rr_intervals[i] = np.nan
                force_next = True
            last_interval = rr

        # wrong areas
        num_wrong_area_peaks = 0
        if not ignore_wrong_areas:
            wi = 0
            ri = 1      # first interval is always nan
            while wi < len(wrong_areas) and ri < len(r_peaks):
                wrong_area = wrong_areas[wi]
                while ri < len(r_peaks):
                    x = r_peaks[ri]
                    if wrong_area[0] <= x <= wrong_area[1]:     # in wrong area
                        rr_intervals[ri] = np.nan
                        num_wrong_area_peaks += 1
                    elif x > wrong_area[1]:     # behind area, break and wait for next area
                        break
                    ri += 1
                wi += 1

        if plot:
            plt.figure("R peaks to obtain RR intervals")
            plt.plot(self.signal, linewidth=1)
            plt.plot(r_peaks, self.signal[r_peaks], "x", color="green")
            nan_indexes = []
            for irr, rr in enumerate(rr_intervals):
                if np.isnan(rr):
                    nan_indexes.append(irr)
            plt.plot(r_peaks[nan_indexes], self.signal[r_peaks[nan_indexes]], "x", color="red")
            for interval in wrong_areas:
                plt.axvspan(interval[0], interval[1], color='red', alpha=0.5)
            plt.title("R peaks for RR intervals (time before peak).")
            plt.legend(["ECG signal", "ok R peak", "wrong R peak", "wrong area"])
            plt.show()

        # converting to ms
        if self.sample_rate != 1000:
            rr_intervals = rr_intervals * (1000 / self.sample_rate)

        return rr_intervals, num_wrong_area_peaks

    def _filterLowQRSPeaks(self, peaks, smooth_squared_diff, ssd_smooth):
        rtn = []

        for p in peaks:
            if smooth_squared_diff[p] > ssd_smooth[p]:
                rtn.append(p)
        return np.asarray(rtn)

    def _countUpperBound(self, values, percentile):
        """
        IQR for upper bound only.
        :param values: values to use
        :param percentile: percentile to use
        :return: upper bound, outlier indexes (higher values are outliers
        """
        vals = values.copy()
        min_val = vals.min()
        vals -= min_val
        vals_perc = np.percentile(vals, percentile)
        upper_bound = 3 * vals_perc

        outliers = []
        for i, v in enumerate(vals):
            if v > upper_bound:
                outliers.append(i)

        upper_bound += min_val
        return upper_bound, outliers

    def _wrongAreas(self, peaks, outlier_indexes, signal_len):
        intervals = []
        for i in range(len(outlier_indexes)):
            left_index = outlier_indexes[i] - 1
            right_index = outlier_indexes[i] + 1

            if left_index < 0:  # 1. peak is wrong -> start at 0
                left = 0
            else:
                left = peaks[left_index] # start at peak before

            if right_index >= len(peaks):   # last peak is wrong
                right = signal_len
            else:
                right = peaks[right_index]

            if len(intervals) == 0: # append first
                intervals.append([left, right])
            else:
                last_interval = intervals[len(intervals) - 1]
                if last_interval[1] >= left:    # overlap
                    last_interval[1] = right
                else:                           # no overlap
                    intervals.append([left, right])

        return intervals


def _interpolate_nans(rr_intervals, r_peaks, min_group=1):
    """
    correcting nan groups according to samples and current heart rate
    """
    assert len(rr_intervals) == len(r_peaks)
    # multiple nans has to be replaced with equal number of them
    nans = []
    rr_to_rep = []
    for i, rr in enumerate(rr_intervals):
        if np.isnan(rr):
            nans.append(i)
        else:
            if len(nans) >= min_group:   # nans has to be replaced according to samples and current heart rate
                leftok = nans[0] - 1
                rightok = i
                if leftok >= 0:
                    samples = r_peaks[rightok] - r_peaks[leftok] - rr_intervals[rightok]    # righ ok interval has taken some samples
                    interval = (rr_intervals[rightok] + rr_intervals[leftok]) / 2  # average interval
                    needed = max(1, int(samples / interval))
                    if needed != len(nans):
                        rr_to_rep.append({"left": nans[0], "right": nans[len(nans) - 1],
                                          "needed": needed})
            nans.clear()        # next nans group
    nans.clear()    # clean up

    # rr_to_rep contains what has to be fixed
    curr_index = 0
    rr_rep = []
    for rep in rr_to_rep:
        before = rr_intervals[curr_index:rep["left"]]
        rr_rep.extend(before)
        rr_rep.extend(np.full(rep["needed"], np.nan, dtype=float))
        curr_index = rep["right"] + 1

    if curr_index < len(rr_intervals):
        rr_rep.extend(rr_intervals[curr_index:])    # after nan groups
    return rr_rep


def _interpolate_nan_values(rr_intervals, method="linear"):
    series = pd.Series(rr_intervals)
    x = series.interpolate(method=method).dropna()
    return x.values

