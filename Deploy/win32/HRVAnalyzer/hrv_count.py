import hrvanalysis

def print_result(topic: str, result: dict):
    print(topic)
    for key, value in result.items():
        print("\t",key, " - ",  value)


def run(interpolated_rr):
    # time-domain methods
    time_domain = hrvanalysis.extract_features.get_time_domain_features(interpolated_rr)
    print_result("HRV - time-domain methods", time_domain)

    # frequency-domain methods
    frequency_domain = hrvanalysis.extract_features.get_frequency_domain_features(interpolated_rr)
    print_result("HRV - frequency-domain methods", frequency_domain)
